module articleprocessor

go 1.23.2

require (
	github.com/alecthomas/chroma v0.10.0
	github.com/gomarkdown/markdown v0.0.0-20241105142532-d03b89096d81
	github.com/gosimple/slug v1.14.0
	github.com/mattn/go-sqlite3 v1.14.24
)

require (
	github.com/dlclark/regexp2 v1.11.4 // indirect
	github.com/gosimple/unidecode v1.0.1 // indirect
)
