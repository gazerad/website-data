package elements

import (
	"flag"
	"log"
	"os"
	"strings"

	"articleprocessor/pkg/mdhtmlconverter"
)

type Config struct {
	IsPage        bool
	CodeStyle     string
	ShowCss       bool
	InputFile     string
	IdArticle     int
	Language      string
	ImagePath     string
	Tags          []string
	DataPath      string
	DatabasePath  string
	PathMdFiles   string
	PathHtmlFiles string
}

// Get config by parsing the arguments and retrieving environment variables
func NewConfig() Config {
	var config Config
	// Define flags
	flag.BoolVar(&config.IsPage, "page", false, "Defines if we are converting a page instead of an article, so no database management and all")
	flag.StringVar(&config.CodeStyle, "codestyle", "monokai", "The code style library to be applied by the converter")
	flag.BoolVar(&config.ShowCss, "showcss", false, "Defines if we show code styling CSS classes")
	flag.StringVar(&config.InputFile, "file", "", "Markdown input file name, into $DATA_PATH/articles/md/[language] folder for article, else $DATA_PATH/pages/md/[language]")
	flag.IntVar(&config.IdArticle, "id", 0, "The article id value from the database; set to 0 for a new article")
	flag.StringVar(&config.Language, "lang", "fr", "The article/page language")
	flag.StringVar(&config.ImagePath, "imagepath", "", "The article/page main image path")
	var tagsList string
	flag.StringVar(&tagsList, "tags", "", "Tags list for the article, separator is comma without space")

	// Parse flags, get environment variables and make checks
	flag.Parse()

	log.Printf("Input data are:\n  IsPage: %t\n  CodeCss: %s\n  ShowCss: %t\n  File: %s\n  IdArticle: %d\n  Language: %s\n  Tags: %s\n",
		config.IsPage, config.CodeStyle, config.ShowCss, config.InputFile, config.IdArticle, config.Language, tagsList)

	if config.InputFile == "" {
		log.Fatal("Error: No markdown input file provided (-file option)")
	}

	config.DataPath = os.Getenv("DATA_PATH")
	if config.IsPage {
		config.PathMdFiles = os.Getenv("DATA_PATH") + "/pages/md"
		config.PathHtmlFiles = os.Getenv("DATA_PATH") + "/pages/html"
		return config
	}

	config.Tags = strings.Split(tagsList, ",")
	config.DatabasePath = os.Getenv("DATA_PATH") + "/db/" + os.Getenv("DATABASE_NAME")
	config.PathMdFiles = os.Getenv("DATA_PATH") + "/articles/md"
	config.PathHtmlFiles = os.Getenv("DATA_PATH") + "/articles/html"

	return config
}

// Convert the markdown data to an html file
func (config *Config) HtmlConvertPage(data []byte) (string, error) {
	basename := strings.Split(config.InputFile, ".md")[0]
	outputFilePath := config.PathHtmlFiles + "/" + config.Language + "/" + basename + ".html"
	//dataHtml := mdhtmlconverter.MdToHTMLBasic(data, false)
	dataHtml := mdhtmlconverter.MdToHTMLCodeHighlight(data, config.Language, false, config.CodeStyle, config.ShowCss)
	dataHtmlFinal := updateImgSrcValue(dataHtml, true)
	err := os.WriteFile(outputFilePath, dataHtmlFinal, 0644)
	return outputFilePath, err
}
