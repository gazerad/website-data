package elements

import (
	"fmt"
	"html/template"
	"log"
	"math"
	"os"
	"regexp"
	"strings"
	"unicode/utf8"

	"github.com/gosimple/slug"

	mdhtmlconverter "articleprocessor/pkg/mdhtmlconverter"
	// readingtime "github.com/begmaroman/reading-time"
)

type Article struct {
	Id            int
	IdArticle     int
	TitleSlug     string
	Language      string
	Title         string
	Description   template.HTML
	TableContents template.HTML
	FileName      string
	ImagePath     string
	ReadDuration  int
	HtmlFilePath  string
	Tags          []string
}

type Articles interface {
	AddArticle(article *Article) error
	AddLkArticleTags(article *Article) error
}

func NewArticle(config Config, data []byte) *Article {
	// Initiate the Article object
	var article Article
	article.IdArticle = config.IdArticle
	article.Language = config.Language
	article.ImagePath = config.ImagePath
	article.Tags = config.Tags

	// Determine the article title and its slug
	titleRe := regexp.MustCompile(`^#[\s]+(.*)\n\s*\n`)
	match := titleRe.FindSubmatch(data)
	if match == nil {
		log.Fatal("Error : no title found into the md file")
	}
	article.Title = string(match[1])
	article.TitleSlug = slug.MakeLang(article.Title, article.Language)
	article.FileName = article.TitleSlug + ".html"

	// Define the html article file path from the previous slug
	article.HtmlFilePath = config.PathHtmlFiles + "/" + config.Language + "/" + article.FileName
	log.Printf("Article title slug is %s, and file path is %s\n", article.TitleSlug, article.HtmlFilePath)

	// Determine the article reading time
	article.ReadDuration = fileReadingTime(data)
	log.Printf("Article computed reading time is %d minutes\n", article.ReadDuration)

	// DEPRECATED - description is defined later by updating the database
	/*
		// Determine the article description (from the '## Introduction' part in initial Markdown file)
		introRe := regexp.MustCompile(`(?s)(?m)\n##[\s]+Introduction\n(\s*\n)+(.*?)\n(\s*\n)+[#]+\s`)
		match = introRe.FindSubmatch(data)
		if match != nil {
			// Extract the text between the patterns
			content := bytes.TrimSpace(match[2])
			article.Description = template.HTML(string(mdhtmlconverter.MdToHtml(content)))
		} else {
			article.Description = ""
		}
	*/
	if article.Language == "fr" {
		article.Description = "Article sur un sujet technique : tutoriel, cas d'utilisation, ..."
	} else if article.Language == "en" {
		article.Description = "Article on a technical topic : tutorial, use case, ..."
	} else {
		article.Description = ""
	}

	// Get the table of contents in Markdown and convert it to HTML
	article.TableContents = setTableContents(data, article.Language)

	// Get the article introduction image path from markdown
	// and update it to match with website setup
	article.ImagePath = ""
	subTitleRe := regexp.MustCompile(`(?s)^#[\s]+.*?\n\s*\n(?:.*?)(<img[^>]*>)`)
	match = subTitleRe.FindSubmatch(data)

	if match != nil {
		imgFinalSrc := updateImgSrcValue(match[1], false)
		imgPathRe := regexp.MustCompile(`<img.*src="([A-Za-z0-9_\-\/\.]+)".*(\/>|<\/img>)`)
		matchImgPath := imgPathRe.FindSubmatch(imgFinalSrc)
		article.ImagePath = string(matchImgPath[1])
	}

	return &article
}

// Convert the article from markdown to HTML with code syntax highlighting and write the file
func (article *Article) HtmlConvertArticle(data []byte, codeCss string, showCss bool) error {
	// Removing the title from the markdown input file as it is stored
	// into the db and rendered apart from the rest of the article
	titleRe := regexp.MustCompile(`^#[\s]+.*\n\s*\n`)
	data = titleRe.ReplaceAll(data, []byte(""))

	// Launch the html conversion with code highlight
	dataHtml := mdhtmlconverter.MdToHTMLCodeHighlight(data, article.Language, true, codeCss, showCss)
	// Update the src field value from <img> blocks to set the accurate path
	dataHtmlFinal := updateImgSrcValue(dataHtml, false)
	// remove <span class="err"> wrongly added by the converter
	dataHtmlFinal = removeErrSpan(dataHtmlFinal)
	log.Printf("Writing article to file %s\n", article.HtmlFilePath)
	err := os.WriteFile(article.HtmlFilePath, dataHtmlFinal, 0644)
	return err
}

// Generates the article table of contents as a HTML list and updates TableContents field from Article object
func setTableContents(data []byte, language string) template.HTML {
	// Get the table of contents in Markdown and convert it to HTML
	headingsMdRe := regexp.MustCompile(`(?m)\n([#]{2,4}[\s]+.*)\n`)
	headingsMdData := headingsMdRe.FindAllSubmatch(data, -1)
	headingMdContentRe := regexp.MustCompile(`^([#]+)[\s]+(.*)$`)
	headingNb := 0
	var headingNbCur int
	var tableContents string
	var headingSlugsSlice []string
	for i := range headingsMdData {
		// We get the heading type and text from Markdown data extracted
		headingMd := string(headingsMdData[i][1])
		headingMatches := headingMdContentRe.FindStringSubmatch(headingMd)
		headingType := headingMatches[1]
		headingText := headingMatches[2]
		headingNbCur = utf8.RuneCountInString(headingType)
		// We need to replace ' character with - to match what is done by mdtohtml converter
		headingSlug := slug.MakeLang(strings.ReplaceAll(headingText, "'", "-"), language)
		// We ensure unique ids
		headingSlugDisp := headingSlug
		nbSlugs := countOccurrences(headingSlugsSlice, headingSlug)
		if nbSlugs > 0 {
			headingSlugDisp += fmt.Sprintf("-%d", nbSlugs)
		}
		headingSlugsSlice = append(headingSlugsSlice, headingSlug)

		if headingNb == 0 {
			tableContents = fmt.Sprintf(`
				<ul class="nav bs-docs-sidenav">
					<li>
						<a class="text-decoration-none" href="#%s">%s</a>`, headingSlugDisp, headingText)
		} else {
			if headingNbCur == headingNb {
				tableContents += fmt.Sprintf(`
					</li>
					<li>
						<a class="text-decoration-none" href="#%s">%s</a>`, headingSlugDisp, headingText)
			} else if headingNbCur > headingNb {
				headingNbDiff := headingNbCur - headingNb
				for j := 1; j <= headingNbDiff; j++ {
					tableContents += `
					<ul class="nav">`
				}
				tableContents += fmt.Sprintf(`
					<li>
						<a class="text-decoration-none" href="#%s">%s</a>`, headingSlugDisp, headingText)
			} else {
				tableContents += `
					</li>`
				headingNbDiff := headingNb - headingNbCur
				for j := 1; j <= headingNbDiff; j++ {
					tableContents += `
					</ul>`
				}
				tableContents += fmt.Sprintf(`
					<li>
						<a class="text-decoration-none" href="#%s">%s</a>`, headingSlugDisp, headingText)
			}
		}

		headingNb = headingNbCur
	}
	// Closing all <ul> and <li> opened
	headingNbDiff := headingNbCur - 2
	if headingNbDiff > 0 {
		for j := 1; j <= headingNbDiff; j++ {
			tableContents += `
					</li>
				</ul>`
		}
	}
	tableContents += `
			</li>
		</ul>`

	return template.HTML(tableContents)
}

// Determines a file content reading time from the number of words calculated
func fileReadingTime(data []byte) int {
	// Default value for words read by minute
	wordsPerMinute := 200
	// Count the number of matches of word characters in the content
	wordRegex := regexp.MustCompile(`\w+`)
	matches := wordRegex.FindAll(data, -1)
	wordCount := len(matches)

	// Reading time stats.
	minutesFloat := math.Ceil(float64(wordCount) / float64(wordsPerMinute))
	return int(minutesFloat)
}

// Replace <img> src value to match with the website setup
func updateImgSrcValue(htmlData []byte, isPage bool) []byte {
	// Defining the images path for pages or articles
	var imagesDirPath string
	if isPage {
		imagesDirPath = "/static/images/pages/"
	} else {
		imagesDirPath = "/static/images/articles/"
	}
	// Extract the <img> parts from the html file
	re := regexp.MustCompile(`(<img.*src=")(.*\/)*([A-Za-z0-9_\-]+\/[A-Za-z0-9_\-]+\.[A-Za-z]+".*(\/>|<\/img>))`)

	// Replace the image path
	return re.ReplaceAll(htmlData, []byte("$1"+imagesDirPath+"$3"))
}

// Removes <span class="err"> wrongly added by md to html converter
func removeErrSpan(htmlData []byte) []byte {
	// We first define the group of strings that we don't want to see
	// highlighted as errors
	// capturingGroup := `('|:)`
	capturingGroup := `(.*)`
	// Then we define the regex
	re := regexp.MustCompile(`<span class="err">` + capturingGroup + `<\/span>`)

	return re.ReplaceAll(htmlData, []byte("$1"))
}

// Count occurences of a given string in a slice of string
func countOccurrences(slice []string, target string) int {
	count := 0
	for _, s := range slice {
		if s == target {
			count++
		}
	}
	return count
}

/*
// Converts a slice of byte slices into a string with a separator
func convertSliceByteString(data [][][]byte, separator string) string {
	separatorByte := []byte(separator)
	// We will extract the submatches with the given separator
	var extractedData []byte
	for i := range data {
		extractedData = append(extractedData, append(data[i][1], separatorByte...)...)
	}
	convertedString := string(extractedData)

	// Check if the last character is a separator
	if strings.HasSuffix(convertedString, separator) {
		// Remove the separator character from the end
		convertedString = strings.TrimRight(convertedString, separator)
	}
	return convertedString
}
*/
