package mdhtmlconverter

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/gosimple/slug"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	mdhtml "github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"

	"github.com/alecthomas/chroma"
	chromahtml "github.com/alecthomas/chroma/formatters/html"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
)

type HtmlFormat struct {
	HtmlFormatter  *chromahtml.Formatter
	HighlightStyle *chroma.Style
	Language       string
}

func NewHtmlFormat(language string, codeStyle string, showCss bool) (*HtmlFormat, error) {
	htmlformat := &HtmlFormat{}
	htmlformat.Language = language
	htmlFormatter := chromahtml.New(chromahtml.WithClasses(true), chromahtml.TabWidth(2))
	if htmlFormatter == nil {
		return nil, errors.New("couldn't create html formatter")
	}
	htmlformat.HtmlFormatter = htmlFormatter

	// monokailight, vs, github-dark, monokai, native
	highlightStyle := styles.Get(codeStyle)
	if highlightStyle == nil {
		return nil, errors.New("didn't find style '" + codeStyle + "'")
	}
	htmlformat.HighlightStyle = highlightStyle

	// Writing CSS classes if required
	if showCss {
		err := htmlformat.HtmlFormatter.WriteCSS(os.Stdout, highlightStyle)
		if err != nil {
			return nil, errors.New("couldn't get CSS from style used")
		}
	}

	return htmlformat, nil
}

// based on https://github.com/alecthomas/chroma/blob/master/quick/quick.go
func (htmlformat *HtmlFormat) HtmlHighlight(w io.Writer, source, lang, defaultLang string) error {
	if lang == "" {
		lang = defaultLang
	}
	l := lexers.Get(lang)
	if l == nil {
		l = lexers.Analyse(source)
	}
	if l == nil {
		l = lexers.Fallback
	}
	l = chroma.Coalesce(l)

	it, err := l.Tokenise(nil, source)
	if err != nil {
		return err
	}

	return htmlformat.HtmlFormatter.Format(w, htmlformat.HighlightStyle, it)
}

// Render code blocks with syntax highlighting
func (htmlformat *HtmlFormat) renderCode(w io.Writer, codeBlock *ast.CodeBlock, entering bool) {
	defaultLang := "bash"
	entering = false
	lang := string(codeBlock.Info)
	htmlformat.HtmlHighlight(w, string(codeBlock.Literal), lang, defaultLang)
}

// Render headings (level 2-4) with anchor links containing image
func (htmlformat *HtmlFormat) renderHeader(w io.Writer, header *ast.Heading, entering bool) {
	if entering {
		renderHeadingEnter(w, header, htmlformat.Language)
	} else {
		renderHeadingExit(w, header)
	}
}

// Render heading entering (classes, id, ...)
func renderHeadingEnter(w io.Writer, header *ast.Heading, language string) {
	// Setting the header id to slug format
	header.HeadingID = slug.MakeLang(header.HeadingID, language)
	// Defining the header definition and writing it
	var headerDefStr string
	if header.Level == 2 {
		headerDefStr = `<h%d id="%s" class="mt-5"><span>`
	} else {
		headerDefStr = `<h%d class="fw-bold mt-4 mb-4" id="%s"><span>`
	}
	headerDef := fmt.Sprintf(headerDefStr, header.Level, header.HeadingID)
	io.WriteString(w, headerDef)
}

// Render heading exiting, so adds an anchor link before closing the header and after the header text
func renderHeadingExit(w io.Writer, header *ast.Heading) {
	// We add anchor link for headers from level 2 to 4
	if header.Level >= 2 && header.Level <= 4 {
		linkDef := fmt.Sprintf(` </span><a class="icon-link icon-link-hover text-secondary" href="#%s">`, header.HeadingID)
		svgElem := `
<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-link-45deg" viewBox="0 0 16 16">
	<path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1 1 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4 4 0 0 1-.128-1.287z"/>
	<path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243z"/>
</svg>		
		`
		linkClose := "</a>"
		if header.Level == 2 {
			linkClose = linkClose + "<hr>"
		}
		io.WriteString(w, linkDef+svgElem+linkClose)
	}
	io.WriteString(w, fmt.Sprintf(`</h%d>`, header.Level))
}

// Customized render hook, handling code blocks and headers
func (htmlformat *HtmlFormat) myRenderHook(w io.Writer, node ast.Node, entering bool) (ast.WalkStatus, bool) {
	// Code blocks customized rendering
	if code, ok := node.(*ast.CodeBlock); ok {
		htmlformat.renderCode(w, code, entering)
		return ast.GoToNext, true
	}
	// Headings customized rendering
	if header, ok := node.(*ast.Heading); ok {
		htmlformat.renderHeader(w, header, entering)
		return ast.GoToNext, true
	}

	return ast.GoToNext, false
}

func (htmlformat *HtmlFormat) newCustomizedRender() *mdhtml.Renderer {
	opts := mdhtml.RendererOptions{
		Flags:          mdhtml.CommonFlags,
		RenderNodeHook: htmlformat.myRenderHook,
	}
	return mdhtml.NewRenderer(opts)
}

func MdToHTMLCodeHighlight(md []byte, language string, isTemplate bool, codeStyle string, showCss bool) []byte {
	// create markdown parser with extensions
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock
	p := parser.NewWithExtensions(extensions)

	// Use custom HTML renderer
	htmlformat, err := NewHtmlFormat(language, codeStyle, showCss)
	if err != nil {
		log.Fatal(err)
	}

	renderer := htmlformat.newCustomizedRender()
	htmlOutput := markdown.ToHTML(md, p, renderer)

	// We replace possible double curly brackets {{ }} by {{"{{"}} and {{"}}"}}
	// to avoid template generation error only if the html is not designed
	// to be included into a Go html template
	if !isTemplate {
		htmlOutput = bytes.ReplaceAll(htmlOutput, []byte("{{"), []byte("{{\"{{\"}}"))
		htmlOutput = bytes.ReplaceAll(htmlOutput, []byte(" }}"), []byte(" {{\"}}\"}}"))
	}

	return htmlOutput
}

// Basic conversion Markdown to HTML
func MdToHTMLBasic(md []byte, printAst bool) []byte {
	// create markdown parser with extensions
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock
	p := parser.NewWithExtensions(extensions)
	doc := p.Parse(md)

	if printAst {
		fmt.Print("--- AST tree:\n")
		ast.Print(os.Stdout, doc)
		fmt.Print("\n")
	}

	// create HTML renderer with extensions
	htmlFlags := mdhtml.CommonFlags | mdhtml.HrefTargetBlank
	opts := mdhtml.RendererOptions{Flags: htmlFlags}
	renderer := mdhtml.NewRenderer(opts)

	return markdown.Render(doc, renderer)
}

// Renderer with only default config
func MdToHtml(md []byte) []byte {
	return markdown.ToHTML(md, nil, nil)
}
