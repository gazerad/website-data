package db

import (
	elements "articleprocessor/pkg/elements"
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

type DB struct {
	*sql.DB
}

// Init initializes the database connection
func NewDb(databasePath string) (*DB, error) {
	db, err := sql.Open("sqlite3", databasePath)
	if err != nil {
		return nil, err
	}
	rdb := &DB{db}
	return rdb, nil
}

// Close closes the database connection
func (db *DB) Close() {
	if db.DB != nil {
		db.DB.Close()
	}
}

// Functions used to define DB interface
func (db *DB) Articles() elements.Articles { return &Articles{db} }
