package db

import (
	"fmt"
	"log"
	"strings"

	elements "articleprocessor/pkg/elements"
)

type Articles struct {
	db *DB
}

var _ elements.Articles = (*Articles)(nil)

// AddArticle inserts or replaces an article into the database
func (repo *Articles) AddArticle(article *elements.Article) error {
	// First determine if we add or replace an article, from article.IdArticle field value
	var operation, query string
	log.Printf("Article id indicated is: %d", article.IdArticle)
	if article.IdArticle == 0 {
		operation = "insert"
		query = `
			INSERT INTO articles (id_article, title_slug, language, title, description, table_contents, filename, image_path, read_duration)
			SELECT COALESCE(MAX(id_article), 0) + 1, ?, ?, ?, ?, ?, ?, ?, ? FROM articles;
		`
	} else {
		// We use a replace statement to also handle the case when the article already exists in another language
		operation = "replace"
		query = `
			REPLACE INTO articles (id_article, title_slug, language, title, description, table_contents, filename, image_path, read_duration,
				creation_date, modified_date)
			SELECT ?, ?, ?, ?, COALESCE(a.description, ?), ?, ?, ?, ?, COALESCE(a.creation_date, date()), date()
			FROM articles a
			RIGHT JOIN (SELECT ? as id_article, ? as language) t 
			ON a.id_article = t.id_article AND a.language = t.language;
		`
	}

	stmt, err := repo.db.Prepare(query)

	if err != nil {
		log.Printf("Error preparing the %s query for the article: %v", operation, err)
		return err
	}

	if operation == "insert" {
		log.Printf("Inserting new article %s, language %s", article.TitleSlug, article.Language)
		_, err = stmt.Exec(article.TitleSlug, article.Language, article.Title, article.Description,
			article.TableContents, article.FileName, article.ImagePath, article.ReadDuration)
		if err != nil {
			log.Printf("Error on the %s execution of the article data: %v", operation, err)
			return err
		}
		// We also get the id_article for this newly added article and sets it for the *Article reference provided
		err2 := repo.db.QueryRow("SELECT id_article FROM articles WHERE title_slug = ? AND language = ?", article.TitleSlug, article.Language).Scan(&article.IdArticle)
		if err2 != nil {
			log.Printf("Could not retrieve id_article value from %sed article: %v", operation, err2)
			return err2
		}
		log.Printf("New article id is: %d", article.IdArticle)

	} else {
		log.Printf("Inserting/Replacing data for article %s, language %s, id %d", article.TitleSlug, article.Language, article.IdArticle)
		_, err = stmt.Exec(article.IdArticle, article.TitleSlug, article.Language, article.Title, article.Description, article.TableContents, article.FileName, article.ImagePath, article.ReadDuration,
			article.IdArticle, article.Language)
	}

	if err != nil {
		log.Printf("Error on the %s execution of the article data: %v", operation, err)
		return err
	}

	return nil
}

// AddLkArticleTags handle links between an article and the tags associated
func (repo *Articles) AddLkArticleTags(article *elements.Article) error {
	tags := article.Tags
	log.Printf("There are %d tags to map to the article", len(tags))
	if len(tags) == 0 {
		return nil
	}

	// We first add/replace all links provided
	queryAdd := `
		REPLACE INTO lk_articles_tags (id_article, id_tag)
		SELECT ?, id_tag FROM tags WHERE tag = ? AND language = ?;
	`

	stmt, err := repo.db.Prepare(queryAdd)
	if err != nil {
		log.Println("Error preparing the query for adding article/tags links: ", err)
		return err
	}

	for _, tag := range tags {
		log.Printf("Adding link between %s tag and %s article", tag, article.TitleSlug)
		_, err = stmt.Exec(article.IdArticle, tag, article.Language)
		if err != nil {
			log.Printf("Error on the execution of the article/'%s' tag link adding: %v", tag, err)
			return err
		}
	}

	// We now remove all links that are not valid anymore

	// Generate placeholders for the tags
	placeholders := make([]string, len(tags))
	for i := range tags {
		placeholders[i] = "?"
	}

	queryDel := fmt.Sprintf(`
		DELETE FROM lk_articles_tags
		WHERE lk_articles_tags.id_article = ? 
		AND NOT EXISTS (
			SELECT 1
			FROM tags t
			WHERE t.tag IN (%s)
				AND t.language = ?
				AND lk_articles_tags.id_tag = t.id_tag
		);
	`, strings.Join(placeholders, ", "))

	stmt, err = repo.db.Prepare(queryDel)
	if err != nil {
		log.Println("Error preparing the query to remove article/tags links: ", err)
		return err
	}

	// Construct the arguments for Exec, including article id, tags, and language
	args := make([]interface{}, 0, len(tags)+2)
	args = append(args, article.IdArticle)
	for _, tag := range tags {
		args = append(args, tag)
	}
	args = append(args, article.Language)

	// Execute the statement with arguments
	_, err = stmt.Exec(args...)
	if err != nil {
		log.Println("Error executing the query to remove article/tags links: ", err)
		return err
	}

	return nil
}
