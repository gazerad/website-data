package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"

	db "articleprocessor/pkg/db"
	elements "articleprocessor/pkg/elements"
	// readingtime "github.com/begmaroman/reading-time"
)

func main() {
	// Get config by parsing the arguments and retrieving environment variables
	config := elements.NewConfig()

	// Read the markdown file
	inputFilePath := config.PathMdFiles + "/" + config.Language + "/" + config.InputFile
	data, err := os.ReadFile(inputFilePath)
	check(err, "Error reading "+inputFilePath+" file:")

	// Case of a page markdown file (other than an article)
	// No slug title formatting, no database management, only conversion
	if config.IsPage {
		outputFilePath, err := config.HtmlConvertPage(data)
		check(err, "Error writing file "+outputFilePath)
		log.Printf("Page converted to HTML at this path: %s\n", outputFilePath)
		return
	}

	// Create article object from config and Markdown file data
	article := elements.NewArticle(config, data)

	// Convert the article from markdown to HTML with code syntax highlighting and write the file
	err = article.HtmlConvertArticle(data, config.CodeStyle, config.ShowCss)
	check(err, "Error writing file "+article.HtmlFilePath)
	log.Printf("Article converted to HTML at this path: %s\n", article.HtmlFilePath)

	// Back-uping the sql database
	dbBkpFilePath, err := backupDatabase(config)
	check(err, "Error backing up database to destination "+dbBkpFilePath)
	log.Printf("Database backed up at following destination: %s\n", dbBkpFilePath)

	// Initialize the connection pool.
	log.Printf("Initializing the connection pool from %s database\n", config.DatabasePath)
	db, err := db.NewDb(config.DatabasePath)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	log.Printf("Done\n")

	// Insert/Update the article into the database
	err = db.Articles().AddArticle(article)
	check(err, "Error adding article to the database")

	// Insert/Update the link table between articles and tags
	// We assume that the tags have previously been created
	err = db.Articles().AddLkArticleTags(article)
	check(err, "Error adding the links article/tags to the database")

	log.Printf("Done: article converted to html and added/updated into the database, with its tags")
}

func check(e error, message string) {
	if e != nil {
		fmt.Println(message, e)
		os.Exit(1)
	}
}

func backupDatabase(config elements.Config) (string, error) {
	// Destination folder
	destinationFolder := config.DataPath + "/db/archive"
	// Get current date and time formatted as YYYYMMDD-HHMMSS
	currentDateTime := time.Now().Format("20060102-150405")
	// New filename with the formatted date/time
	dbBkpFileName := fmt.Sprintf("%s_%s", currentDateTime, filepath.Base(config.DatabasePath))
	// Destination file path
	dbBkpFilePath := filepath.Join(destinationFolder, dbBkpFileName)

	// Copy the file to the destination folder with the new filename
	err := copyFile(config.DatabasePath, dbBkpFilePath)
	if err != nil {
		return dbBkpFilePath, err
	}

	return dbBkpFilePath, nil
}

// Copy file function
func copyFile(src, dst string) error {
	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destination.Close()

	_, err = io.Copy(destination, source)
	if err != nil {
		return err
	}

	return nil
}

/*
	// Get table of contents as a string (instead of []byte)
	/*tableContentsMd := ""
	if len(matchedLines) > 0 {
		// Loop over each string
		for i, str := range matchedLines {
			// Append the current string
			tableContentsMd += str

			// If it's not the last string, append a newline character
			if i < len(matchedLines)-1 {
				tableContentsMd += "\n"
			}
		}
	}

*/
