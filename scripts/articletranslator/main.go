package main

import (
	"bufio"
	"bytes"
	"context"
	"flag"
	"fmt"
	"html"
	"log"
	"os"
	"regexp"
	"strings"

	"cloud.google.com/go/translate"
	"golang.org/x/text/language"
)

func main() {
	// Parsing the arguments to get the input file name
	var filename string
	var lang string
	flag.StringVar(&filename, "filename", "", "The input file name, located into input/ folder")
	flag.StringVar(&lang, "lang", "en", "The target language for the translation")
	flag.Parse()

	if filename == "" {
		log.Fatal("Error: markdown input file provided (-filename option)")
	}

	inputFile := "input/" + filename
	outputFile := "output/" + filename

	// Read Markdown file content
	content, err := os.ReadFile(inputFile)
	if err != nil {
		fmt.Printf("Error during file reading : %v\n", err)
		return
	}

	// Init Google Translate client
	ctx := context.Background()
	client, err := translate.NewClient(ctx)
	if err != nil {
		fmt.Printf("Error during Google Translate API client init : %v\n", err)
		return
	}
	defer client.Close()

	// Translate language
	targetLang, err := language.Parse(lang)
	if err != nil {
		fmt.Printf("Error during target language analysis : %v\n", err)
		return
	}

	// Regular expressions to handle exceptions
	linkPattern := regexp.MustCompile(`\[(.*?)\]\((.*?)\)`) // Links [text](URL)
	// codeBlockPattern := regexp.MustCompile("(?s)```.*?```")            // Code blocks ```...```
	inlineCodePattern := regexp.MustCompile("`([^`]*)`")                // Inline code `...`
	imgTagPattern := regexp.MustCompile(`<img[^>]*alt="([^"]*)"[^>]*>`) // <img ... >

	// Translate line by line
	scanner := bufio.NewScanner(bytes.NewReader(content))
	var translatedContent strings.Builder
	inCodeBlock := false

	for scanner.Scan() {
		line := scanner.Text()

		// Code blocks handling
		if strings.HasPrefix(strings.TrimSpace(line), "```") {
			inCodeBlock = !inCodeBlock
			translatedContent.WriteString(line + "\n")
			continue
		}
		if inCodeBlock {
			translatedContent.WriteString(translateCommentsInCode(line, client, ctx, targetLang) + "\n")
			continue
		}

		// Markdown links translation
		line = linkPattern.ReplaceAllStringFunc(line, func(match string) string {
			return handleLinks(match, client, ctx, targetLang)
		})

		// Image blocks translation
		line = imgTagPattern.ReplaceAllStringFunc(line, func(match string) string {
			return translateAltText(match, client, ctx, targetLang)
		})

		// Markdown comments translation
		if strings.HasPrefix(strings.TrimSpace(line), ">") {
			comment := strings.TrimPrefix(line, ">")
			translations, err := client.Translate(ctx, []string{comment}, targetLang, nil)
			if err == nil {
				translatedComment := html.UnescapeString(translations[0].Text)
				comment = restoreSpecialCharacters(translatedComment)
			}
			translatedContent.WriteString(">" + comment + "\n")
			continue
		}

		// Inline code is not translated
		line = inlineCodePattern.ReplaceAllStringFunc(line, func(match string) string {
			return match
		})

		// Translate normal lines
		translations, err := client.Translate(ctx, []string{line}, targetLang, nil)
		if err != nil {
			fmt.Printf("Error during translation : %v\n", err)
			return
		}

		// Convert HTML entities into normal characters
		translatedLine := restoreSpecialCharacters(html.UnescapeString(translations[0].Text))
		translatedContent.WriteString(translatedLine + "\n")
	}

	if err := scanner.Err(); err != nil {
		fmt.Printf("Error during content reading : %v\n", err)
		return
	}

	// Écrire le contenu traduit dans le fichier de sortie
	err = os.WriteFile(outputFile, []byte(translatedContent.String()), 0644)
	if err != nil {
		fmt.Printf("Error during translated file writing : %v\n", err)
		return
	}

	fmt.Printf("File successfully translated : %s\n", outputFile)
}

// Restore some special characters after translation
func restoreSpecialCharacters(text string) string {
	replacer := strings.NewReplacer(
		"$", "\\$",
	)
	return replacer.Replace(text)
}

// Only translate comments into code blocks
func translateCommentsInCode(line string, client *translate.Client, ctx context.Context, targetLang language.Tag) string {
	// Identify comments at the end of a line too
	// pattern := `(.*)(?:#[ ]+|//[ ]+)(.+)`
	pattern := `(.*)(#[ ]+|//[ ]+)(.+)`
	re := regexp.MustCompile(pattern)
	match := re.FindStringSubmatch(line)

	//if strings.HasPrefix(strings.TrimSpace(line), "//") || strings.HasPrefix(strings.TrimSpace(line), "#") {
	// translations, err := client.Translate(ctx, []string{line}, targetLang, nil)
	if len(match) > 1 {
		translations, err := client.Translate(ctx, []string{match[3]}, targetLang, nil)
		if err == nil {
			translatedLine := match[1] + match[2] + translations[0].Text
			return translatedLine
		}
	}
	return line
}

// Handle Markdown links translation
func handleLinks(match string, client *translate.Client, ctx context.Context, targetLang language.Tag) string {
	linkPattern := regexp.MustCompile(`\[(.*?)\]\((.*?)\)`)
	matches := linkPattern.FindStringSubmatch(match)
	if len(matches) == 3 {
		text := matches[1]
		url := matches[2]

		// Only translate text
		translations, err := client.Translate(ctx, []string{text}, targetLang, nil)
		if err == nil {
			text = html.UnescapeString(translations[0].Text)
		}

		return fmt.Sprintf("[%s](%s)", text, url)
	}
	return match
}

// Translate alt attribute from <img ... >
func translateAltText(match string, client *translate.Client, ctx context.Context, targetLang language.Tag) string {
	imgTagPattern := regexp.MustCompile(`alt="([^"]*)"`)
	return imgTagPattern.ReplaceAllStringFunc(match, func(altMatch string) string {
		altPattern := regexp.MustCompile(`alt="([^"]*)"`)
		altText := altPattern.FindStringSubmatch(altMatch)
		if len(altText) == 2 {
			translations, err := client.Translate(ctx, []string{altText[1]}, targetLang, nil)
			if err == nil {
				return fmt.Sprintf(`alt="%s"`, html.UnescapeString(translations[0].Text))
			}
		}
		return altMatch
	})
}
