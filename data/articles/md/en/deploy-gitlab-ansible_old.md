# Continuous deployment with Gitlab and Ansible

## Introduction

Installing an application, especially a third-party one, is generally not such a big deal, and even the upgrade can be made easier with the use of Docker Compose for instance. But at some point, sticking with manual tasks for deployments, upgrades and configuration changes can be cumbersome and error prone, especially if a rollback is required with unversionned configuration.

On the other side, when we only have to manage a handful containers for an application deployed on-premise (which happens more often than we think :-) ), we don't really need to push a Kubernetes setup which will essentially increase the maintenance costs and the on-boarding learning curve for newcomers.

Ansible offers the ability to control the configuration of an application on a given infrastructure and several environments, Gitlab is a source code management (SCM) interface that allows to configure CI/CD (continuous integration/continuous deployment). 

We will see from a use case how to make an accurate use of both tools in order to make simultaneously safe and quick deployments.

## Use case definition

In the article, we will consider the deployment of a monitoring solution based on Grafana on DEV and PROD environments running on Linux servers. We currently have a docker compose setup but with neither SCM nor automated deployment.

We would like to achieve a new setup where :
- The configuration is handled as code with a SCM project
- The configuration is dynamically generated for a pool of environments (here DEV and PROD)
- Deployments are triggerred by push/tag events on the SCM tool, and more precisely prepared to be launched manually from the SCM UI.
- No user access is required on the infrastructure to deploy.

This the perfect situation to make use of **Ansible** in order to manage the configuration, statically and dynamically, and to deploy on a set of servers/environments. **Gitlab** is also a logical choice as a SCM tool that also allows to automate tasks triggered by SCM events by defining pipelines.

<img src="static/images/articles/deploy-gitlab-ansible/image-2023-3-10_13-10-48.png" alt="Continous deployment architecture" width="650"/>

This way, we can :
- ensure that the configuration is backed-up and versionned which makes the previous setup immediately available and deployable in case of rollback
- limit as much as possible user access to the infrastructure in order to perform manual tasks

Consequently we can achieve improvements in both process safety and efficency with the ability to deploy quickly what we precisely want.

## Configure Ansible SSH and Gitlab

### Generate the SSH key pair used by Ansible

Ansible is based on SSH to deploy on the servers associated to each environment of a given application.
Consequently, for each of these servers, we need to generate a SSH key pair that will be recognized by ansible user to process the deployment.

The server where the SSH key pair does not matter. For practical reason concerning our use case, we will name each key as below:

```
ansible_[application name]_[environment]
```

Please find below the commands to execute on any server to generate the SSH key.
We choose an empty passphrase as it makes the deployment harder to automatize if there is a one and requires the manual input of a sensitive password.

```sh
ssh-keygen -t ed25519

Generating public/private ed25519 key pair.
Enter file in which to save the key (~/.ssh/id_ed25519): ~/.ssh/ansible_[application name]_[environment]
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in ~/.ssh/ansible_app_env.
Your public key has been saved in ~/.ssh/ansible_app_env.pub.
The key fingerprint is:
SHA256:/dBKUd8TExQDWoxpNTKt8xesipzAbscR9wtINJg2t9I root@SR-F1-GITRUN2
The key's randomart image is:
+--[ED25519 256]--+
|        o  oB=oBo|
|       = + ==+..+|
|      . = =.. o..|
|       . E *   o.|
|      . S B = . .|
|       o + = + . |
|      . + = + o  |
|       o * . .   |
|      . .        |
+----[SHA256]-----+
```

Now, we have the private/public SSH key pair available :
- `~/.ssh/ansible_[application name]_[environment]`
- `~/.ssh/ansible_[application name]_[environment].pub`

For example, for Grafana application and dev environment :
- `~/.ssh/ansible_grafana_dev`
- `~/.ssh/ansible_grafana_dev.pub`

### Convert SSH key pair to base64 format

In order to be added as Gitlab project variables, we need to convert the keys to a one-line string, so we use base64 encoding for this.

From the key pair previously generated :

```sh
APPLICATION=""  # Put the name of the application here
ENVIRONMENT=""  # Environment name
 
# Each time, we save the string generated
cat ~/.ssh/ansible_${APPLICATION}_${ENVIRONMENT} | base64 -w 0
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 
cat ~/.ssh/ansible_${APPLICATION}_${ENVIRONMENT}.pub | base64 -w 0
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

### Add encoded SSH keys to Gitlab project variables

Now we will have to edit the CI/CD settings of our Gitlab project dedicated to applications deployment in order to add the encoded keys as variables.

First please note, for our use case, that the variable names must follow this rule, for a given APPLICATION  application and ENVIRONMENT  environment (each in **upper case**) :

```
# Private key
[APPLICATION]_[ENVIRONMENT]_SSH_PRIVATE_KEY_BASE64
 
# Public key
[APPLICATION]_[ENVIRONMENT]_SSH_PUBLIC_KEY_BASE64
```

For example, with Grafana application and dev environment :

    Private key : GRAFANA_DEV_SSH_PRIVATE_KEY_BASE64
    Public key : GRAFANA_DEV_SSH_PUBLIC_KEY_BASE64

Then, we fill these variables with these features (described [here](https://docs.gitlab.com/ee/ci/variables/#for-a-project)), depending on the environment :
- **prod** : protect and mask the variable
- **dev** : only mask variable

## Configure target servers

The following tasks will need to be applied to each server associated to an application / environment. So we will consider below that we are connected as root  user on of these servers, on which Docker is already installed.

### Create ansible user

Ansible needs a user with the following features on the server where the application will be deployed :

- sudo rights
  - without password : otherwise, the automation will be hard to handle.
  - to all commands : limiting commands is discouraged by [Ansible documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_privilege_escalation.html#privilege-escalation-must-be-general) as Ansible wraps the expected commands in Python and executes many others.
- no user password authentication : only SSH key will be used.

So we execute the following commands as root :

```sh
# Log as root

# Create ansible user, with sudo rights on all commands
useradd -m ansible -s /bin/bash
usermod -aG sudo ansible
echo "ansible ALL=(ALL) NOPASSWD:ALL" | tee /etc/sudoers.d/ansible
 
# Remove password authentication for ansible user
passwd -l ansible
```

### Install required libraries

Ansible might need Python libraries on the target servers, depending on the modules that are used.

Currently, for any application relying on a docker compose setup, we will need the following ones : `python3-pip` and `docker-compose`.

```sh
# Install required python libaries for ansible
apt-get update
apt-get install python3-pip
pip install docker-compose
```

### Provide the SSH public key used to authenticate

In order to authorize the connection initiated with the SSH keys generated for each application/environment, we will need to add the relevant one into ansible user configuration.

Consequently, we will create `~/.ssh` folder and update `~/.ssh/authorized_keys` file.

```sh
# Add public key used by ansible master to authorized keys of ansible user
su - ansible
mkdir -p ~/.ssh
echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" | tee -a ~/.ssh/authorized_keys
exit
```

### Authenticate to Gitlab dependency proxy repository (optional)

As the application services will have to pull their related Docker images, we might experience a [pull rate limit exceed](https://docs.docker.com/docker-hub/download-rate-limit/) from Docker hub caused by all the Gitlab instance pipelines that use Docker images. Hopefully, Gitlab has introduced the [dependency proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/) repository which is a pull-through-cache for public registry images from Docker hub. This feature requires an authentication relying on a [Gitlab token](https://docs.gitlab.com/ee/security/token_overview.html) so we will see how to manage this authentication.

First, make sure that a **credentials store** is configured for Docker as we are on a Linux server. This step is not required for a Windows setup but we are not considering this case.

To do so, please check that the following command can be executed:

```sh
docker-credential-pass list
```

If not, you will have to follow the process below :

```sh
# Login as root

# Download docker-credential-pass from https://github.com/docker/docker-credential-helpers/releases
VERSION="v0.8.1" # Check for latest release

wget https://github.com/docker/docker-credential-helpers/releases/download/$VERSION/docker-credential-pass-$VERSION.linux-amd64
 
# Move it to /usr/bin folder with accurate rights
mv docker-credential-pass-$VERSION.linux-amd64 /usr/bin/docker-credential-pass
chmod u+x /usr/bin/docker-credential-pass
 
# Install gpg and pass
apt-get install gpg pass
 
# Generate gpg key and enter information
gpg --generate-key
 
# The following output should be generated
# pub   rsa3072 2018-10-07 [SC] [expires: 2020-10-06]
#       1234567890ABCDEF1234567890ABCDEF12345678
# Copy the key contained at the last line and init pass with it
GPG_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
pass init $GPG_KEY
 
# Set the next password to "pass is initialized" (without quotes).
pass insert docker-credential-helpers/docker-pass-initialized-check
 
# Now, `docker-credential-pass list` command should produce an output.
# Create now a ~/.docker/config.json with the following content
{
"credsStore": "pass"
}
 
# docker login should now work
```

Once done, you will just have to login once with a [Gitlab token](https://docs.gitlab.com/ee/security/token_overview.html) that has enough rights to pull Docker images from the Gitlab dependency proxy repository.

```sh
# replace with your own instance FQDN
docker login gitlab.example.com
```

## Manage the deployment on the Gitlab project

### Application content

As [previously](#add-encoded-ssh-keys-to-gitlab-project-variables) stated, we consider that we have created a project named `Applications Deployment` and containing for our use a case a folder named `Grafana` that will manage the deployment of this application containing several services.

We are going to show below the tree structure of the project that will be detailed later, and which is inspired by this [recommended strategy](https://www.digitalocean.com/community/tutorials/how-to-manage-multistage-environments-with-ansible#ansible-recommended-strategy-using-groups-and-multiple-inventories) in case of multiple environments and Ansible general [best practices](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#content-organization)

```
├── grafana
│   ├── config
│   │   ├── nginx
│   │   │   ├── conf.d
│   │   │   ├── streams-enabled
│   │   │   │   └── influxdb-proxy.conf
│   │   │   └── nginx.conf
│   │   ├── postgres
│   │   │   └── init
│   │   │       └── init_robot_db.sh
│   │   └── prometheus
│   │       └── prometheus.yml
│   ├── environments
│   │   ├── dev
│   │   │   ├── group_vars
│   │   │   │   └── all
│   │   │   │       ├── cross_env_vars.yml -> ../../../cross_env_vars.yml
│   │   │   │       ├── vars.yml
│   │   │   │       ├── vault.yml
│   │   │   │       └── versions.yml -> ../../../versions.yml
│   │   │   └── hosts
│   │   ├── prod
│   │   │   ├── group_vars
│   │   │   │   └── all
│   │   │   │       ├── cross_env_vars.yml -> ../../../cross_env_vars.yml
│   │   │   │       ├── vars.yml
│   │   │   │       ├── vault.yml
│   │   │   │       └── versions.yml -> ../../../versions.yml
│   │   │   └── hosts
│   │   ├── cross_env_vars.yml
│   │   └── versions.yml
│   ├── templates
│   │   ├── default.conf.j2
│   │   └── docker-compose.yml.j2
│   └── playbook-deploy.yml
```

#### Playbook definition

```
├── grafana
    └── playbook-deploy.yml
```

The Ansible playbook dedicated to Grafana deployment is defined at root level into `grafana/`application folder, and is expected to be named `playbook-deploy.yml`.

It contains the list of the tasks executed by the [Ansible playbook](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_intro.html).

Basically, for Grafana, an application based on docker compose with multiple services, we want to :

- stop docker compose
- back-up databases
- synchronize the services configuration with the one published in Gitlab
- deploy the docker compose setup
- start docker compose

All of these tasks need to be executed on each environment (dev and prod) and the corresponding servers (`grafana-dev.example.com`, `grafana.example.com` for instance).

Let's see briefly how an Ansible playbook is made, from `playbook-deploy.yml` example.

First, the name section defines global parameters for the whole playbook.

```yaml
- name: Deploy Grafana Docker Compose setup   # playbook name
  hosts: grafana                              # list of hosts 
  become: true                                # commands are executed with sudo
  gather_facts: false                         # facts and not needed here
```

So in this case, we have defined :

- `name` : the playbook name
- `hosts` : the list of hosts defined by the application name grafana. Then, Ansible associates the related servers (here, only one) from the environment provided when the playbook is launched.
- `become` : with true value, indicates that commands are executed with sudo on the target server (required to manage docker compose). More details [here](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_privilege_escalation.html).
- `gather_facts` : indicates whether Ansible retrieves the [facts](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_vars_facts.html) or not (default is true). Here, in order to speed up the playbook, we do not retrieve them as they are not needed.

Then, we define the list of tasks which will be executed by the playbooks.

```yaml
tasks:
  - name: Get remote docker-compose.yml content (also to determine if the application has already been installed)
    slurp:
      src: "{{ application_path }}/docker-compose.yml"
    register: remote_content_encoded
    ignore_errors: yes
 
  - name: "Set variables: remote and local PostgreSQL major versions, PostgreSQL Robot/InfluxDB database dump locations and force Robot database upgrade."
    set_fact:
      remote_major_postgres_version: "{{ remote_content_encoded.content | b64decode | regex_findall('image:.*postgres:([0-9]+)') | first if not remote_content_encoded.failed else 0 }}"
      local_major_postgres_version: "{{ postgres_version | regex_findall('([0-9]+)') | first }}"
      postgres_dump_filename: "robot_db_{{ lookup('pipe', 'date +%Y%m%d%H%M%S') }}.tar"
      influxdb_dump_dirname: "influxdb_{{ lookup('pipe', 'date +%Y%m%d%H%M%S') }}"
      force_postgres_upgrade: "{{ 'N' if force_postgres_upgrade is not defined }}"
      new_application: "{{ true if remote_content_encoded.failed else false }}"
    delegate_to: localhost
 
  - debug:
      msg: "Remote PostgreSQL major version is {{ remote_major_postgres_version }}. New one is {{ local_major_postgres_version }}. Dump filename is {{ postgres_dump_filename }}. Force Robot database upgrade: {{ force_postgres_upgrade }}. InfluxDB dump folder is: {{ influxdb_dump_dirname }}"
    delegate_to: localhost
 
  - debug:
      msg: "No docker compose was available into {{ application_path }} directory so we will consider that we install the application for the first time."
    delegate_to: localhost
    when: new_application
 
  - name: Stop docker compose
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: absent
    when: not new_application
 
  - name: Create application, configuration and dump folders if they don't exist
    file:
      path: "{{ item }}"
      state: directory
    loop:
      - "{{ application_path }}"
      - "{{ postgres_conf_dir }}"
      - "{{ prometheus_conf_dir }}"
      - "{{ nginx_conf_dir }}"
      - "{{ postgres_dump_dir }}"
      - "{{ influxdb_dump_dir }}"
 
  - name: Start PostgreSQL service to make Robot database back-up
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      services: postgres_db
      state: present
    when: not new_application
 
  - name: Sleep for 3 seconds before backing-up the database
    wait_for:
      timeout: 3
    delegate_to: localhost
    when: not new_application
 
  - name: Back-up PostgreSQL Robot database
    shell: |
      docker exec -i postgres_db pg_dump -U {{ robot_db_user }} -F c {{ robot_db_name }} > {{ postgres_dump_dir }}/{{ postgres_dump_filename }}
    args:
      chdir: "{{ application_path }}"
      creates: "{{ postgres_dump_dir }}/{{ postgres_dump_filename }}"
    when: not new_application
 
  - name: Start InfluxDB service to make InfluxDB database back-up
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      services: influxdb
      state: present
    when: not new_application
 
  - name: Sleep for 3 seconds before backing-up the database
    wait_for:
      timeout: 3
    delegate_to: localhost
    when: not new_application
 
  - name: Back-up InfluxDB database
    shell: |
      docker exec -i influxdb influx backup /var/local/{{ influxdb_dump_dirname }}
    args:
      chdir: "{{ application_path }}"
      creates: "{{ influxdb_dump_dir }}/{{ influxdb_dump_dirname }}"
    when: not new_application
 
  - name: Stop docker compose
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: absent
    when: not new_application
 
  - name: Deploy configuration folders
    copy:
      src: "{{ item.src }}"
      dest: "{{ item.dest }}"
      mode: "{{ item.mode}}"
    loop:
      - src: config/nginx/ # Ending src folder with / means that the content of the src folder is copied into the dest path and not the src folder itself
        dest: "{{ nginx_conf_dir }}/"
        mode: '640'
      - src: config/postgres/
        dest: "{{ postgres_conf_dir }}/"
        mode: '644'
      - src: config/prometheus/
        dest: "{{ prometheus_conf_dir }}/"
        mode: '644'
 
  - name: Deploy docker-compose file
    template:
      src: templates/docker-compose.yml.j2
      dest: "{{ application_path }}/docker-compose.yml"
      mode: '600'
 
  - name: Deploy nginx configuration file
    template:
      src: templates/default.conf.j2
      dest: "{{ nginx_conf_path }}"
      mode: '640'
 
  - name: Remove current database volume in case of major Robot database PostgreSQL version upgrade
    docker_volume:
      name: grafana_postgres_db_data
      state: absent
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Start PostgreSQL service to restore the back-up in case of major PostgreSQL version upgrade
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      services: postgres_db
      state: present
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Sleep for 3 seconds before restoring the database in case of major PostgreSQL version upgrade
    wait_for:
      timeout: 3
    delegate_to: localhost
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Restore the database back-up in case of major PostgreSQL version upgrade
    shell: |
      docker exec -i postgres_db pg_restore -U {{ robot_db_user }} -v -d {{ robot_db_name }} < {{ postgres_dump_dir }}/{{ postgres_dump_filename }}
    args:
      chdir: "{{ application_path }}"
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Stop docker compose in case of major PostgreSQL version upgrade
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: absent
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Start docker compose
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: present
```

We will not detail here all the tasks but let's remark that :

- **variables** : they are expressed with the [Jinja2](https://palletsprojects.com/p/jinja/) syntax `{{ variable_name }}`. They are defined into the playbook configuration that we will detail later, and fromsome tasks under `set_fact` directive.
- **modules** : Ansible provide modules that allow to execute some tasks without providing the exact bash command. The module is defined just under the task name. We are using here the following ones:
  - [`slurp`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/slurp_module.html) : retrieve a file content (base64 encoded) from the remote host into a variable
  - [`set_fact`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/set_fact_module.html) : allows to set host variables and facts that can be reused into the playbook.
  - [`docker_compose`](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_compose_module.html) : executes docker compose commands
  - [`wait_for`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html) : waits during some times before continuing the playbook (here we define a 3 seconds waiting time by setting `timeout: 3`)
  - [`shell`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html) : executes the shell command indicated on the target host
  - [`copy`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html) : copy src file/folder to dest remote location.
  - [`template`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html) : expands a template file by filling Jinja2 variables with their values, and copies them to the target hosts the same way as with copy module.
  - [`docker_volume`](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_volume_module.html) : executes operations on docker volumes on the target host

- **loops** : used with copy module here, allows to loop the same task on multiple items defined by a list of fields. Then, the item is identified by `{{item}}` variable into the task definition.

  ```yaml
      - name: Deploy configuration folders
        copy:
          src: "{{ item.src }}"
          dest: "{{ item.dest }}"
          mode: "{{ item.mode}}"
        loop:
          - src: config/nginx/ # Ending src folder with / means that the content of   the src folder is copied into the dest path and not the src folder itself
            dest: "{{ nginx_conf_dir }}/"
            mode: '640'
          - src: config/postgres/
            dest: "{{ postgres_conf_dir }}/"
            mode: '644'
          - src: config/prometheus/
            dest: "{{ prometheus_conf_dir }}/"
            mode: '644'
  ```

For each iteration of the loop, `{{item}}` is filled by `src`, `dest`, and `mode` values defined for each item under `loop` directive.

- **[conditions](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_conditionals.html)** : defines the conditions under which a task is executed, under `when` keyword.

- `delegate_to` : only indicates on which other host than the application/environment remote one we want to execute the task. Here, we sometimes prefer executing on `localhost` tasks that does not require to be executed on the target host.

#### Inventory and host variables

In order to make the playbook code be able to work on different sets of hosts/variables depending on the environment chosen, we need to define an [inventory](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html).

The inventory is defined by a set of folders/subfolders under `environments` folder.

For example here :

```
├── grafana
│   ├── environments
│   │   ├── dev
│   │   │   ├── group_vars
│   │   │   │   └── all
│   │   │   │       ├── cross_env_vars.yml -> ../../../cross_env_vars.yml
│   │   │   │       ├── vars.yml
│   │   │   │       ├── vault.yml
│   │   │   │       └── versions.yml -> ../../../versions.yml
│   │   │   └── hosts
│   │   ├── prod
│   │   │   ├── group_vars
│   │   │   │   └── all
│   │   │   │       ├── cross_env_vars.yml -> ../../../cross_env_vars.yml
│   │   │   │       ├── vars.yml
│   │   │   │       ├── vault.yml
│   │   │   │       └── versions.yml -> ../../../versions.yml
│   │   │   └── hosts
│   │   ├── cross_env_vars.yml
│   │   └── versions.yml
```

##### Environment configuration

The two environments, `dev` and `prod`, have the same tree structure which are following [suggested practices](https://www.digitalocean.com/community/tutorials/how-to-manage-multistage-environments-with-ansible#ansible-recommended-strategy-using-groups-and-multiple-inventories).

- `group_vars/all` : contains the definition of variables required by the environment.
The `all/` folder name indicates that these variables apply to every host defined into `hosts` file that we will detail after.
Here we can see that we have the following content:
  - `vars.yml` : contains variables specific to the environment.

    ```yaml
    # FQDN for services others than Grafana
    influxdb_fqdn: influxdb-dev.example.com
    prometheus_fqdn: prometheus-dev.example.com
     
    # Certificate base name
    cert_name: grafana-dev_example_com
     
    # Grafana Azure AD client ID (for authentication, depends on server)
    grafana_azuread_client_id: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx
    ```

  - `vault.yml` : contains "vaulted" variables, which are passwords encrypted with [Ansible vault](https://docs.ansible.com/ansible/latest/vault_guide/index.html). The data can be published on Gitlab as they are indeed encrypted.

    ```yaml
    vault_influxdb_admin_password: !vault |
            $ANSIBLE_VAULT;1.1;AES256
            613935353262346335373831356134333935373634333530643134393536336333633834643  73566
            3664363237313538323465623265633437323563303636610a6661653262373861626266636  16538
            626263656330623331643063353765383331363331343961646339383663393336393234343  66134
            3537336261346639310a6366616437316630613930373733333035626433346638313635306  66638
            333635656463613031343262383531393336663561383636643636653239386130323935316  33065
            6638613034633162653538626434363137396136366438383839
    
    ```

  - symbolic links : they refer to files containing variables which have the same values in all environments.

- `hosts` : this file contains the hosts definition for the application and the environment selected.
We first have to indicate `[application]` key word, and then we list on the same line :
  - the application name
  - the remote host name
  - the user that Ansible will use to connect to the server and execute the playbook.

  There can be here several servers related to an environment but in our case, we only have one server defined, for example for dev environment :

  ```ini
  [application]
  grafana ansible_host=grafana-dev.example.com ansible_user=ansible
  ```

  _NB_ : we have chose here the `.ini` format for this file but it could have been written in YAML, Ansible accepts both formats to define the playbook configuration.

##### Global configuration

We also have two files that contain identical variables definitions for all the environments. As previously explained, they are replicated into each environment configuration by a **symbolic link**.

- `cross_env_vars.yml` : simply contains static variables definitions that apply to all environments.
**IMPORTANT** : there is a section which applies vaulted variables to another variable name. This was done to avoid filling this file with vaulted passwords, which would have made it difficult to read.

```yaml
# Application, dump, configuration and log paths
application_path: /var/opt/grafana
postgres_dump_dir: "{{ application_path }}/dump/postgres"
influxdb_dump_dir: "{{ application_path }}/dump/influxdb"
postgres_conf_dir: "{{ application_path }}/postgres"
prometheus_conf_dir: "{{ application_path }}/prometheus"
prometheus_conf_path: "{{ application_path }}/prometheus/prometheus.yml"
nginx_conf_dir: "{{ application_path }}/nginx"
nginx_conf_path: "{{ application_path }}/nginx/conf.d/default.conf"
nginx_log_path: "{{ application_path }}/nginx/log"
 
# Grafana Azure AD URLs
grafana_azuread_auth_url: "https://login.microsoftonline.com/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx/oauth2/v2.0/authorize"
grafana_azuread_token_url: "https://login.microsoftonline.com/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx/oauth2/v2.0/token"
 
# Robot DB definition
robot_db_name: robot_db
robot_db_user: robot_user
 
# Passwords from vault file
influxdb_admin_password: "{{ vault_influxdb_admin_password }}"
influxdb_admin_token: "{{ vault_influxdb_admin_token }}"
mysql_robot_db_password: "{{ vault_mysql_robot_db_password }}"
mysql_root_password: "{{ vault_mysql_root_password }}"
postgres_user_password: "{{ vault_postgres_user_password }}"
postgres_robot_db_password: "{{ vault_postgres_robot_db_password }}"
pgadmin4_password: "{{ vault_pgadmin4_password }}"
grafana_admin_password: "{{ vault_grafana_admin_password }}"
grafana_azuread_client_secret: "{{ vault_grafana_azuread_client_secret }}"
```

- `versions.yml` : contains the versions of the **services** defined into the docker compose file. This is this file that needs to be updated for a version upgrade of the services.

```yaml
influxdb_version: 2.6.0
mysql_version: 8
postgres_version: 15
pgadmin4_version: 2023-03-06-1
grafana_version: 9.3.6
prometheus_version: v2.42.0
node_exporter_version: v1.5.0
nginx_version: 1.23.3
```

#### Templates (dynamic configuration)

```
├── grafana
│   ├── templates
│   │   ├── default.conf.j2
│   │   └── docker-compose.yml.j2
```

After having described how variables were defined for each environment of the application, we will see now that they can be used to generate dynamic configuration.

To do so, a `templates/` folder has been created at the root of the application directory, and it contains template files including **Jinja2 style variables** `{{ variable }}` populated by Ansible process.

Usually, only the two following configuration items will depend on variables and require a template file :

- **Docker compose** : docker-compose.yml file deployed on the target servers depends on many variables, like Docker images versions, credentials, applications paths.
- **Nginx reverse proxy** : a few elements depend on the environment, like all server names that are used by the application services.

Please find below an extract from a templated `docker-compose.yml` file as an example.

```yaml
influxdb:
  # Image is retrieved from Gitlab dependency proxy to avoid Docker hub
  # rate limit exceed; but in most cases, Docker hub image can be used
  image: gitlab.example.com/projects/dependency_proxy/containers/influxdb:{{ influxdb_version }}
  container_name: influxdb
  restart: always
  volumes:
    - influxdb_data:/var/lib/influxdb2
    - influxdb_conf:/etc/influxdb2
    # Volume for back-ups
    - {{ application_path }}/dump/influxdb:/var/local/dump
  networks:
  # InfluxDB UI must be reachable from the outside using the reverse proxy so 'monitoring' network is also added
    - databases
    - monitoring
  environment:
    # InfluxDB will create the specified database and an admin account with the specified credentials.
    # More info: https://github.com/influxdata/influxdata-docker/pull/102
    # https://www.influxdata.com/blog/running-influxdb-2-0-and-telegraf-using-docker/
    DOCKER_INFLUXDB_INIT_MODE: "setup"
    DOCKER_INFLUXDB_INIT_USERNAME: "admin"
    DOCKER_INFUXDB_INIT_PASSWORD: "{{ influxdb_admin_password }}"
    DOCKER_INFLUXDB_INIT_ORG: "monitoring"
    DOCKER_INFLUXDB_INIT_BUCKET: "gitlab"
    DOCKER_INFLUXDB_INIT_ADMIN_TOKEN: "{{ influxdb_admin_token }}"
```

#### Static configuration

```
├── grafana
│   ├── config
│   │   ├── nginx
│   │   │   ├── conf.d
│   │   │   ├── streams-enabled
│   │   │   │   └── influxdb-proxy.conf
│   │   │   └── nginx.conf
│   │   ├── postgres
│   │   │   └── init
│   │   │       └── init_robot_db.sh
│   │   └── prometheus
│   │       └── prometheus.yml
```

We also have services that will require a static configuration (which means not dependant of any variable) to be deployed.

All of this will be located under `config` folder, with a directory for each of the services.

### Encrypt credentials with Ansible vault

As we have previously seen, credentials are encrypted with [Ansible vault](https://docs.ansible.com/ansible/latest/vault_guide/index.html).

The encryption is based on a vault password which is available as `ANSIBLE_VAULT_PASSWORD` variable from Gitlab `Applications Deployment` project settings.

In order to encrypt a password, you can pull [Cytopia Ansible](https://hub.docker.com/r/cytopia/ansible) docker image and launch a bash into the container, which will avoid you to directly install Ansible on your work environment whereas you do not really need it. You can execute the following commands locally as long as Docker is installed.

```sh
# Pull the docker image
docker pull cytopia/ansible:latest-tools
 
# Create a container and launch a bash into it
docker run -it --rm cytopia/ansible:latest-tools /bin/bash
 
[INFO] root> /bin/bash
bash-5.1#
```

Inside the container, copy `ANSIBLE_VAULT_PASSWORD` Gitlab variable value into a file named `.vault_pass`

Then, set `ANSIBLE_VAULT_PASSWORD_FILE` environment variable and you will be able to get the vault encryption for your password.

```sh
# Open .vault_pass file and put here the vault password
vi .vault_pass
 
# Set ANSIBLE_VAULT_PASSWORD_FILE environment variable
export ANSIBLE_VAULT_PASSWORD_FILE=./.vault_pass
 
# Encrypt the password
ansible-vault encrypt_string 'my_password'
Encryption successful
!vault |
          $ANSIBLE_VAULT;1.1;AES256
          30356138393065643632353532623533346230393835386563336161373334366333313130333162
          3332643466393664636333303864373339393633333733660a353634613030313030636232613661
          30396636336663303733623762313231316130633338643964383261313835343238313662363433
          6564353666343838330a633339383534386435613464356531303266666339343239333763363536
          3561
```

Now, you can copy the vaulted password into the related configuration file that we previously saw.

### Gitlab CI deployment pipeline

#### Pipeline workflow

The Gitlab CI pipeline workflow is expected to follow theses guidelines :
- Only two stages exist : `deploy_dev` and `deploy_prod` 
- All jobs are manual
- Jobs are created for each application with modified files. For example, if we only modify Grafana application configuration (`grafana/` folder in the project) only Grafana deployment job will be created.
- On latest / release steps (commit on `main` branch / creation of a release tag) : dev environment jobs (`deploy_dev` stage) are located before prod environment ones (`deploy_prod` stage) BUT they are allowed to fail so it is possible to directly launch prod environment deployment jobs.

  <img src="static/images/articles/deploy-gitlab-ansible/image-2023-3-10_12-37-28.png" alt="Gitlab CI pipleline" width="600"/>

#### Gitlab CI code

##### Template hidden job

Applications Deployment projet `.gitlab-ci.yml` file applies the previous workflow, using `deploy-playbook-template` template job to be redefined for each application/environment/stage job.

```yaml
stages:
  - deploy_dev
  - deploy_prod

variables:
  ANSIBLE_FORCE_COLOR: 'true' # get Ansible colors displayed on Gitlab UI

# We will only only define the latest step that runs on commits pushed on main branch
.rules_ref:
  latest: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

###############################################################################################################
# Jobs definition

# Template job that MUST NOT BE RUN and will be extended
deploy-playbook-template:
  image: ${CI_DEPENDENCY_PROXY_SERVER}/projects/dependency_proxy/containers/cytopia/ansible:latest-tools
  stage: .pre
  rules:
    - when: never
  script:
    # Deduct variables from job name if not provided into target job config
    - if [ "X$APPLICATION_NAME" == "X" ]; then APPLICATION_NAME=$(echo "$CI_JOB_NAME" | awk -F "_|-" '{print $(NF-1)}'); fi
    - echo "INFO - Application to be deployed is $APPLICATION_NAME"
    - if [ "X$ENVIRONMENT" == "X" ]; then ENVIRONMENT=$(echo "$CI_JOB_NAME" | awk -F "_|-" '{print $NF}'); fi
    - echo "INFO - Environment is $ENVIRONMENT"
    # We expect SSH key variables to match the following name : ${APPLICATION_NAME}_${ENVIRONMENT}_SSH_(PRIVATE|PUBLIC)_KEY_BASE64
    - SSH_PRIVATE_KEY_BASE64_VARNAME=$(echo "${APPLICATION_NAME}_${ENVIRONMENT}_SSH_PRIVATE_KEY_BASE64" | tr '[:lower:]' '[:upper:]')
    - eval "SSH_PRIVATE_KEY_BASE64=\${$SSH_PRIVATE_KEY_BASE64_VARNAME}"
    - SSH_PUBLIC_KEY_BASE64_VARNAME=$(echo "${APPLICATION_NAME}_${ENVIRONMENT}_SSH_PUBLIC_KEY_BASE64" | tr '[:lower:]' '[:upper:]')
    - eval "SSH_PUBLIC_KEY_BASE64=\${$SSH_PUBLIC_KEY_BASE64_VARNAME}"
    # Test if required variables are filled
    - |-
      if [ "X$APPLICATION_NAME" == "X" ] || [ "X$ENVIRONMENT" == "X" ] || [ "X$SSH_PRIVATE_KEY_BASE64" == "X" ] || [ "X$SSH_PUBLIC_KEY_BASE64" == "X" ]
      then
        echo "ERROR - One of the required variable was missing. Please check your configuration."
        exit 1
      fi
    # Setup SSH key pair that will be used to authenticate against target server
    - echo "INFO - Adding SSH key pair provided by Gitlab project variables to local SSH configuration"
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY_BASE64" | base64 -d > ~/.ssh/id_ed25519
    - chmod 600 ~/.ssh/id_ed25519
    - echo "$SSH_PUBLIC_KEY_BASE64" | base64 -d > ~/.ssh/id_ed25519.pub
    # Add target host fingerprint to local known_hosts
    # We consider here potentially multiple hosts related to the application (not the current use case though)
    - APPLICATION_SERVER_FQDN_LIST=$(grep $APPLICATION_NAME ${APPLICATION_NAME}/environments/${ENVIRONMENT}/hosts | awk 'BEGIN{RS=" "; FS="ansible_host="}NF>1{print $NF}')
    - echo "INFO - Adding host fingerprints to local known_host for $APPLICATION_NAME application servers FQDN list ($ENVIRONMENT environment) - $(echo $APPLICATION_SERVER_FQDN_LIST)"
    - for APPLICATION_SERVER_FQDN in $APPLICATION_SERVER_FQDN_LIST; do ssh-keyscan -H $APPLICATION_SERVER_FQDN >> ~/.ssh/known_hosts; done
    # Go to application folder from Gitlab project
    - cd $APPLICATION_NAME
    # Setup Ansible vault configuration
    - echo "INFO - Setting up Ansible vault configuration"
    - echo "$ANSIBLE_VAULT_PASSWORD" > .vault_pass
    - export ANSIBLE_VAULT_PASSWORD_FILE=./.vault_pass
    # Launch the Ansible playbook
    - |-
      if [ "$FORCE_POSTGRES_UPGRADE" == "Y" ]
      then
        echo "INFO - Executing the deployment playbook with $ENVIRONMENT environment inventory and the Postgres DB upgrade forced."
        echo "ansible-playbook --extra-vars="force_postgres_upgrade=Y" -i environments/$ENVIRONMENT playbook-deploy.yml"
        ansible-playbook --extra-vars="force_postgres_upgrade=Y" -i environments/$ENVIRONMENT playbook-deploy.yml
      else
        echo "INFO - Executing the deployment playbook with $ENVIRONMENT environment inventory."
        echo "ansible-playbook -i environments/$ENVIRONMENT playbook-deploy.yml"
        ansible-playbook -i environments/$ENVIRONMENT playbook-deploy.yml
      fi

```

The job gives the ability to provide FORCE_POSTGRES_UPGRADE variable (from the Gitlab pipeline interface for example) if we want to force the possible Postgres database upgrade in the playbook.

It is based on the following variables depending on the application/environment :

- `APPLICATION_NAME` : preferably deducted from the job name (example : check-deploy-**grafana**-dev) or provided as variable in the job configuration
- `ENVIRONMENT` : preferably deducted from the job name (example : check-deploy-grafana-**dev**) or provided as variable in the job configuration
- `SSH_PRIVATE_KEY_BASE64_VARNAME` : gets `[APPLICATION]_[ENVIRONMENT_SSH_PRIVATE_KEY_BASE64` project variable value
- `SSH_PUBLIC_KEY_BASE64_VARNAME` : gets `[APPLICATION]_[ENVIRONMENT- _SSH_PUBLIC_KEY_BASE64` project variable value
- `ANSIBLE_VAULT_PASSWORD` : the master vault password used to decrypt the credentials (Gitlab project variable)
- `FORCE_POSTGRES_UPGRADE` (optional) : to force the possible Postgres database upgrade in the playbook

##### Application deployment jobs

If we want to add a new application to the pipeline scope, we just have to extend the `deploy-playbook-template` hidden job, define under which rules the jobs are launched, and set the application name into the job names.

```yaml

#######################
# Grafana application #
#######################

latest-deploy-grafana-dev:
  extends: deploy-playbook-template
  stage: deploy_dev
  rules:
    - if: !reference [.rules_ref, latest]
      changes:
        - grafana/**/*
      when: manual
  allow_failure: true
 
latest-deploy-grafana-prod:
  extends: deploy-playbook-template
  stage: deploy_prod
  rules:
    - if: !reference [.rules_ref, latest]
      changes:
        - grafana/**/*
      when: manual
```

#### Pipeline output

When we execute a job, the output will be as below (with Ansible colors).

<img src="static/images/articles/deploy-gitlab-ansible/image-2023-3-10_13-1-21.png" alt="Gitlab CI pipleline" width="100%"/>

We can remark a few key words which indicates Ansible status for each task :

- `ok` : indicates that the task did not produce any change on the server (for example copying identical file)
- `changed` : the task produced changes on the servers (modifying a file, starting docker compose, ...)
- `skipping` : the task was not executed because conditions (when) were not matched
- `fatal` : task execution resulted in an error; stops all the playbook by default, or the use of `ignore_errors: true` allows to bypass the error