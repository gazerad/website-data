```bash
192.168.80.1 - - [26/Apr/2024:12:46:13 +0000] "GET /fr HTTP/1.1" 200 18102 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36"
```

```json
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "c7b4b28e8f11097f3cbda76fd405d49e345b5b62fbf0bebc6a3d4da2deaa54d0",
            "SandboxKey": "/run/user/1002/docker/netns/c7b4b28e8f11",
            "Ports": {
                "443/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "443"
                    },
                    {
                        "HostIp": "::",
                        "HostPort": "443"
                    }
                ],
                "80/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "80"
                    },
                    {
                        "HostIp": "::",
                        "HostPort": "80"
                    }
                ]
            },
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "",
            "Gateway": "",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "",
            "IPPrefixLen": 0,
            "IPv6Gateway": "",
            "MacAddress": "",
            "Networks": {
                "mywebsite-frontend": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": [
                        "reverse_proxy",
                        "reverse_proxy"
                    ],
                    "MacAddress": "02:42:c0:a8:50:04",
                    "NetworkID": "6940c25f0126bf2c5f47f31dba5a7e28a41aae95de26cf89ed016ba9c6607c80",
                    "EndpointID": "663afbb7b833561079df9175866d61c44c12cd7a0bfab3153aadc50f0655d63f",
                    "Gateway": "192.168.80.1",
                    "IPAddress": "192.168.80.4",
                    "IPPrefixLen": 20,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "DriverOpts": null,
                    "DNSNames": [
                        "reverse_proxy",
                        "377a2557a45c"
                    ]
                },
                "mywebsite-monitoring": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": [
                        "reverse_proxy",
                        "reverse_proxy"
                    ],
                    "MacAddress": "02:42:c0:a8:60:05",
                    "NetworkID": "85c61b1242db356197f0aa0dcdf372187a933eceab943878904f75618bff2dda",
                    "EndpointID": "e0276063e07eafb26032c5888568e872ef949cf638de9aa65d01ee7f484ac045",
                    "Gateway": "192.168.96.1",
                    "IPAddress": "192.168.96.5",
                    "IPPrefixLen": 20,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "DriverOpts": null,
                    "DNSNames": [
                        "reverse_proxy",
                        "377a2557a45c"
                    ]
                }
            }
        }
```

https://docs.docker.com/engine/security/rootless/

IPAddress shown in docker inspect is namespaced inside RootlessKit's network namespace. This means the IP address is not reachable from the host without nsenter-ing into the network namespace.
Host network (docker run --net=host) is also namespaced inside RootlessKit.

https://docs.docker.com/engine/security/rootless/#docker-run--p-does-not-propagate-source-ip-addresses

https://github.com/rootless-containers/rootlesskit/blob/v2.0.0/docs/network.md

https://gitlab.com/abologna/kubevirt-and-kvm/-/blob/master/Networking.md