# Déploiement continu avec Gitlab et Ansible

## Introduction

L'installation d'une application, en particulier d'une application tierce, n'est généralement pas un problème majeur, et même la mise à niveau peut être facilitée par l'utilisation de Docker Compose, par exemple. Mais à un moment donné, s'en tenir à des tâches manuelles pour les déploiements, les mises à niveau et les changements de configuration peut être lourd et sujet à des erreurs, en particulier si un retour en arrière est nécessaire avec une configuration non versionnée.

D'un autre côté, lorsque nous n'avons à gérer qu'une poignée de conteneurs pour une application déployée sur site (ce qui arrive plus souvent qu'on ne le pense :-) ), nous n'avons pas vraiment besoin de pousser une configuration Kubernetes qui augmentera essentiellement les coûts de maintenance et la courbe d'apprentissage pour les nouveaux arrivants.

Ansible offre la possibilité de contrôler la configuration d'une application sur une infrastructure donnée et plusieurs environnements, Gitlab est une interface de gestion de code source (SCM) qui permet de configurer la partie CI/CD (intégration continue/déploiement continu). 

Nous verrons à partir d'un cas d'utilisation comment faire un usage précis des deux outils afin de réaliser des déploiements à la fois sûrs et rapides.

## Définition du cas d'utilisation

Dans cet article, nous allons considérer le déploiement d'une solution de monitoring Grafana sur des environnements DEV et PROD comprenant chacun un serveur Linux. Nous avons actuellement une configuration docker compose mais sans SCM ni déploiement automatisé.

Nous aimerions obtenir une nouvelle configuration où :
- La configuration est gérée _as code_ avec un projet SCM.
- La configuration est générée dynamiquement pour un ensemble d'environnements (ici DEV et PROD)
- Les déploiements sont déclenchés par des événements push/tag sur l'outil SCM, et sont plus précisément préparés pour être lancés manuellement à partir de l'interface SCM.
- Aucun accès utilisateur n'est requis sur l'infrastructure à déployer.

C'est la situation idéale pour utiliser **Ansible** afin de gérer la configuration, statiquement et dynamiquement, et de déployer sur un ensemble de serveurs/environnements. **Gitlab** est également un choix logique en tant qu'outil SCM qui permet également d'automatiser les tâches déclenchées par les événements SCM en définissant des pipelines.

<img src="static/images/articles/deploy-gitlab-ansible/image-2023-3-10_13-10-48.png" alt="Architecture de déploiement continu" width="650"/>

De cette manière, nous pouvons :
- nous assurer que la configuration est sauvegardée et versionnée, ce qui rend la configuration précédente immédiatement disponible et déployable en cas de retour en arrière
- limiter autant que possible l'accès des utilisateurs à l'infrastructure afin d'effectuer des tâches manuelles.

Par conséquent, nous pouvons améliorer la sécurité et l'efficacité des processus en ayant la possibilité de déployer rapidement ce que nous voulons précisément.

## Configurer Ansible SSH et Gitlab

### Générer la paire de clefs SSH utilisée par Ansible

Ansible se base sur SSH pour déployer sur les serveurs associés à chaque environnement d'une application donnée.
Par conséquent, pour chacun de ces serveurs, nous devons générer une paire de clefs SSH qui sera reconnue par l'utilisateur d'Ansible pour effectuer le déploiement.

Le serveur sur lequel se trouve la paire de clefs SSH n'a pas d'importance. Pour des raisons pratiques concernant notre cas d'utilisation, nous nommerons chaque clef comme suit :

```
ansible_[nom de l'application]_[environnement]
```

Vous trouverez ci-dessous les commandes à exécuter sur n'importe quel serveur pour générer la clef SSH.
Nous choisissons une passphrase vide car elle rend le déploiement plus difficile à automatiser s'il y en a une, et nécessite la saisie manuelle d'un mot de passe sensible.

```sh
ssh-keygen -t ed25519

Generating public/private ed25519 key pair.
Enter file in which to save the key (~/.ssh/id_ed25519): ~/.ssh/ansible_[application name]_[environment]
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in ~/.ssh/ansible_app_env.
Your public key has been saved in ~/.ssh/ansible_app_env.pub.
The key fingerprint is:
SHA256:/dBKUd8TExQDWoxpNTKt8xesipzAbscR9wtINJg2t9I root@SR-F1-GITRUN2
The key's randomart image is:
+--[ED25519 256]--+
|        o  oB=oBo|
|       = + ==+..+|
|      . = =.. o..|
|       . E *   o.|
|      . S B = . .|
|       o + = + . |
|      . + = + o  |
|       o * . .   |
|      . .        |
+----[SHA256]-----+
```

Maintenant, nous avons la paire de clefs SSH privée/publique disponible :
- `~/.ssh/ansible_[nom de l'application]_[environnement]`
- `~/.ssh/ansible_[nom de l'application]_[environnement].pub`

Par exemple, pour l'application Grafana et l'environnement de développement :
- `~/.ssh/ansible_grafana_dev`
- `~/.ssh/ansible_grafana_dev.pub`

### Convertir la paire de clefs SSH au format base64

Afin d'être ajoutées en tant que variables de projet Gitlab, nous devons convertir les clefs en une chaîne de caractères d'une ligne, nous utilisons donc l'encodage base64 pour cela.

A partir de la paire de clefs précédemment générée :

```sh
APPLICATION=""  # Mettez le nom de l'application ici
ENVIRONMENT=""  # Nom de l'environnement
 
# A chaque fois, nous sauvegardons la chaîne générée
cat ~/.ssh/ansible_${APPLICATION}_${ENVIRONNEMENT} | base64 -w 0
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 
cat ~/.ssh/ansible_${APPLICATION}_${ENVIRONNEMENT}.pub | base64 -w 0
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

### Ajouter les clefs SSH encodées aux variables du projet Gitlab

Nous allons maintenant devoir éditer les paramètres CI/CD de notre projet Gitlab dédié au déploiement d'applications afin d'ajouter les clefs encodées en tant que variables.

Notez tout d'abord que dans notre cas, les noms de variables doivent suivre la règle suivante, pour une application APPLICATION et un environnement ENVIRONMENT donnés (chacun en **majuscules**) :

```
# clef privée
[APPLICATION]_[ENVIRONNEMENT]_SSH_PRIVATE_KEY_BASE64
 
# clef publique
[APPLICATION]_[ENVIRONNEMENT]_SSH_PRIVATE_KEY_BASE64 # CLEF PUBLIQUE
```

Par exemple, avec l'application Grafana et l'environnement dev :

    Clef privée : GRAFANA_DEV_SSH_PRIVATE_KEY_BASE64
    Clef publique : GRAFANA_DEV_SSH_PUBLIC_KEY_BASE64

Ensuite, nous remplissons ces variables avec les caractéristiques suivantes (décrites [ici](https://docs.gitlab.com/ee/ci/variables/#for-a-project)), en fonction de l'environnement :
- **prod** : protège et masque la variable
- **dev** : masque uniquement la variable

## Configurer les serveurs cibles

Les tâches suivantes devront être appliquées à chaque serveur associé à une application / un environnement. Nous considérerons donc ci-dessous que nous sommes connectés en tant qu'utilisateur root sur l'un de ces serveurs, sur lequel Docker est déjà installé.

### Créer un utilisateur ansible

Ansible a besoin d'un utilisateur avec les caractéristiques suivantes sur le serveur où l'application sera déployée :

- droits sudo
  - sans mot de passe : sinon, l'automatisation sera difficile à gérer.
  - pour toute commande : limiter les commandes est déconseillé par la [documentation Ansible] (https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_privilege_escalation.html#privilege-escalation-must-be-general) car Ansible encapsule les commandes attendues en Python et en exécute beaucoup d'autres.
- pas d'authentification par mot de passe : seule la clef SSH sera utilisée.

Nous exécutons donc les commandes suivantes en tant que root :

```sh
# Se loguer en tant que root

# Créer un utilisateur ansible, avec les droits sudo sur toutes les commandes
useradd -m ansible -s /bin/bash
usermod -aG sudo ansible
echo "ansible ALL=(ALL) NOPASSWD:ALL" | tee /etc/sudoers.d/ansible
 
# Supprimer l'authentification par mot de passe pour l'utilisateur ansible
passwd -l ansible
```

### Installer les bibliothèques nécessaires

Ansible peut avoir besoin de bibliothèques Python sur les serveurs cibles, en fonction des modules utilisés.

Actuellement, pour toute application reposant sur une configuration docker-compose, nous aurons besoin des bibliothèques suivantes : `python3-pip` et `docker-compose`.

```sh
# Installer les librairies python requises pour ansible
apt-get update
apt-get install python3-pip
pip install docker-compose
```

### Fournir la clef publique SSH utilisée pour l'authentification

Afin d'autoriser la connexion initiée avec les clefs SSH générées pour chaque application/environnement, nous aurons besoin d'ajouter la clef pertinente dans la configuration de l'utilisateur ansible.

Par conséquent, nous allons créer le dossier `~/.ssh` et mettre à jour le fichier `~/.ssh/authorized_keys`.

```sh
# Ajouter la clef publique utilisée par ansible master aux clefs autorisées de l'utilisateur ansible
su - ansible
mkdir -p ~/.ssh
echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" | tee -a ~/.ssh/authorized_keys
exit
```

### Authentification au dépôt dependency proxy Gitlab (optionnel)

Nous pourrions rencontrer un [pull rate limit exceed](https://docs.docker.com/docker-hub/download-rate-limit/) de Docker hub causé par tous les pipelines d'instances Gitlab qui utilisent des images Docker. Heureusement, Gitlab a introduit le dépôt [dependency proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/) qui est un _pull-through-cache_ pour les images du registre public à partir de Docker hub. Cette fonctionnalité nécessite une authentification reposant sur un [Gitlab token](https://docs.gitlab.com/ee/security/token_overview.html), nous allons donc voir comment gérer cette authentification.

Tout d'abord, il faut s'assurer qu'un **credentials store** est configuré pour Docker car nous sommes sur un serveur Linux. Cette étape n'est pas nécessaire pour une configuration Windows mais nous ne considérons pas ce cas.

Pour cela, vérifiez que la commande suivante peut être exécutée :

```sh
docker-credential-pass list
```

Si ce n'est pas le cas, vous devrez suivre le processus suivant :

```sh
# Se connecter en tant que root

# Téléchargez docker-credential-pass depuis https://github.com/docker/docker-credential-helpers/releases
VERSION="v0.8.1" # Vérifier la dernière version

wget https://github.com/docker/docker-credential-helpers/releases/download/$VERSION/docker-credential-pass-$VERSION.linux-amd64
 
# Le déplacer dans le dossier /usr/bin avec les droits appropriés
mv docker-credential-pass-$VERSION.linux-amd64 /usr/bin/docker-credential-pass
chmod u+x /usr/bin/docker-credential-pass
 
# Installer gpg et pass
apt-get install gpg pass
 
# Générer la clef gpg et entrer les informations
gpg --generate-key
 
# La sortie suivante devrait être générée
# pub rsa3072 2018-10-07 [SC] [expires : 2020-10-06]
# 1234567890ABCDEF1234567890ABCDEF12345678
# Copier la clef contenue dans la dernière ligne et initier le passage avec elle
GPG_KEY="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
pass init $GPG_KEY

# Définir le prochain mot de passe à "pass is initialized" (sans les guillemets).
pass insert docker-credential-helpers/docker-pass-initialized-check
 
# Maintenant, la commande `docker-credential-pass list` devrait produire une sortie.
# Créez maintenant un ~/.docker/config.json avec le contenu suivant
{
"credsStore" : "pass"
}
 
# Le login docker devrait maintenant fonctionner
```

Une fois cela fait, vous n'aurez plus qu'à vous connecter une fois avec un [Gitlab token] (https://docs.gitlab.com/ee/security/token_overview.html) qui a suffisamment de droits pour extraire des images Docker du registre _dependency proxy_ de Gitlab.

```sh
# remplacez par le FQDN de votre instance instance
docker login gitlab.example.com
```

## Gérer le déploiement sur le projet Gitlab

### Contenu de l'application

Comme [précédemment](#add-encoded-ssh-keys-to-gitlab-project-variables) indiqué, nous considérons que nous avons créé un projet nommé `Applications Deployment` et contenant pour notre cas d'utilisation un dossier nommé `Grafana` qui va gérer le déploiement de cette application contenant plusieurs services.

Nous allons montrer ci-dessous l'arborescence du projet qui sera détaillée plus tard, et qui s'inspire de cette [stratégie recommandée](https://www.digitalocean.com/community/tutorials/how-to-manage-multistage-environments-with-ansible#ansible-recommended-strategy-using-groups-and-multiple-inventories) en cas d'environnements multiples et des [bonnes pratiques](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#content-organization) générales d'Ansible.

```
├── grafana
│ ├─── config
│ │ ├─── nginx
│ │ ├─── conf.d
│ │ ├─── streams-enabled
│ │ │ └─── influxdb-proxy.conf
│ │ └─── nginx.conf
│ │ ├── postgres
│ │ └─── init
| │ └─── init_robot_db.sh
│ │ └── prometheus
│ │ └── prometheus.yml
│ ├─── environments
│ │ ├─── dev
│ │ ├─── group_vars
│ │ └──── all
│ │ │ ├─── cross_env_vars.yml -> ../../../../cross_env_vars.yml
│ │ │ ├─── vars.yml
│ │ │ ├─── vault.yml
│ │ │ └─── versions.yml -> ../../../versions.yml
│ │ └─── hosts
│ │ ├─── prod
│ │ ├─── group_vars
│ │ └──── all
│ │ │ ├─── cross_env_vars.yml -> ../../../../cross_env_vars.yml
│ │ │ ├─── vars.yml
│ │ │ ├─── vault.yml
│ │ │ └─── versions.yml -> ../../../versions.yml
│ │ └─── hosts
│ │ ├── cross_env_vars.yml
│ │ └─── versions.yml
│ ├─── templates
│ │ ├─── default.conf.j2
│ │ └── docker-compose.yml.j2
│ └─── playbook-deploy.yml
```

#### Définition du playbook

```
├── grafana
    └── playbook-deploy.yml
```

Le playbook Ansible dédié au déploiement de Grafana est défini au niveau racine dans le dossier de l'application `grafana/`, et est censé être nommé `playbook-deploy.yml`.

Il contient la liste des tâches exécutées par le [playbook Ansible](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_intro.html).

Fondamentalement, pour Grafana, une application basée sur docker compose avec de multiples services, nous voulons :

- arrêter docker compose
- sauvegarder les bases de données
- synchroniser la configuration des services avec celle publiée sur Gitlab
- déployer la configuration de docker compose
- démarrer docker compose

Toutes ces tâches doivent être exécutées sur chaque environnement (dev et prod) et sur les serveurs correspondants (`grafana-dev.example.com`, `grafana.example.com` par exemple).

Voyons brièvement comment un playbook Ansible est créé, à partir de l'exemple `playbook-deploy.yml`.

Tout d'abord, la section name définit les paramètres globaux pour l'ensemble du playbook.

```yaml
- name : Deploy Grafana Docker Compose setup # nom du playbook
  hosts : grafana # liste des hôtes 
  become : true # les commandes sont exécutées avec sudo
  gather_facts : false # les facts ne sont pas nécessaires ici
```

Donc dans ce cas, nous avons défini :

- `name` : le nom du playbook
- `hosts` : la liste des hôtes définis par le nom de l'application grafana. Ensuite, Ansible associe les serveurs associés (ici, un seul) à partir de l'environnement fourni lors du lancement du playbook.
- `become` : avec une valeur `true`, indique que les commandes sont exécutées en tant que sudo sur le serveur cible (nécessaire pour gérer docker compose). Plus de détails [ici](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_privilege_escalation.html).
- `gather_facts` : indique si Ansible récupère les [facts](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_vars_facts.html) ou non (la valeur par défaut est true). Ici, afin d'accélérer le playbook, nous ne les récupérons pas car ils ne sont pas nécessaires.

Ensuite, nous définissons la liste des tâches qui seront exécutées par les playbooks.

```yaml
tasks:
  - name: Get remote docker-compose.yml content (also to determine if the application has already been installed)
    slurp:
      src: "{{ application_path }}/docker-compose.yml"
    register: remote_content_encoded
    ignore_errors: yes
 
  - name: "Set variables: remote and local PostgreSQL major versions, PostgreSQL Robot/InfluxDB database dump locations and force Robot database upgrade."
    set_fact:
      remote_major_postgres_version: "{{ remote_content_encoded.content | b64decode | regex_findall('image:.*postgres:([0-9]+)') | first if not remote_content_encoded.failed else 0 }}"
      local_major_postgres_version: "{{ postgres_version | regex_findall('([0-9]+)') | first }}"
      postgres_dump_filename: "robot_db_{{ lookup('pipe', 'date +%Y%m%d%H%M%S') }}.tar"
      influxdb_dump_dirname: "influxdb_{{ lookup('pipe', 'date +%Y%m%d%H%M%S') }}"
      force_postgres_upgrade: "{{ 'N' if force_postgres_upgrade is not defined }}"
      new_application: "{{ true if remote_content_encoded.failed else false }}"
    delegate_to: localhost
 
  - debug:
      msg: "Remote PostgreSQL major version is {{ remote_major_postgres_version }}. New one is {{ local_major_postgres_version }}. Dump filename is {{ postgres_dump_filename }}. Force Robot database upgrade: {{ force_postgres_upgrade }}. InfluxDB dump folder is: {{ influxdb_dump_dirname }}"
    delegate_to: localhost
 
  - debug:
      msg: "No docker compose was available into {{ application_path }} directory so we will consider that we install the application for the first time."
    delegate_to: localhost
    when: new_application
 
  - name: Stop docker compose
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: absent
    when: not new_application
 
  - name: Create application, configuration and dump folders if they don't exist
    file:
      path: "{{ item }}"
      state: directory
    loop:
      - "{{ application_path }}"
      - "{{ postgres_conf_dir }}"
      - "{{ prometheus_conf_dir }}"
      - "{{ nginx_conf_dir }}"
      - "{{ postgres_dump_dir }}"
      - "{{ influxdb_dump_dir }}"
 
  - name: Start PostgreSQL service to make Robot database back-up
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      services: postgres_db
      state: present
    when: not new_application
 
  - name: Sleep for 3 seconds before backing-up the database
    wait_for:
      timeout: 3
    delegate_to: localhost
    when: not new_application
 
  - name: Back-up PostgreSQL Robot database
    shell: |
      docker exec -i postgres_db pg_dump -U {{ robot_db_user }} -F c {{ robot_db_name }} > {{ postgres_dump_dir }}/{{ postgres_dump_filename }}
    args:
      chdir: "{{ application_path }}"
      creates: "{{ postgres_dump_dir }}/{{ postgres_dump_filename }}"
    when: not new_application
 
  - name: Start InfluxDB service to make InfluxDB database back-up
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      services: influxdb
      state: present
    when: not new_application
 
  - name: Sleep for 3 seconds before backing-up the database
    wait_for:
      timeout: 3
    delegate_to: localhost
    when: not new_application
 
  - name: Back-up InfluxDB database
    shell: |
      docker exec -i influxdb influx backup /var/local/{{ influxdb_dump_dirname }}
    args:
      chdir: "{{ application_path }}"
      creates: "{{ influxdb_dump_dir }}/{{ influxdb_dump_dirname }}"
    when: not new_application
 
  - name: Stop docker compose
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: absent
    when: not new_application
 
  - name: Deploy configuration folders
    copy:
      src: "{{ item.src }}"
      dest: "{{ item.dest }}"
      mode: "{{ item.mode}}"
    loop:
      - src: config/nginx/ # Ending src folder with / means that the content of the src folder is copied into the dest path and not the src folder itself
        dest: "{{ nginx_conf_dir }}/"
        mode: '640'
      - src: config/postgres/
        dest: "{{ postgres_conf_dir }}/"
        mode: '644'
      - src: config/prometheus/
        dest: "{{ prometheus_conf_dir }}/"
        mode: '644'
 
  - name: Deploy docker-compose file
    template:
      src: templates/docker-compose.yml.j2
      dest: "{{ application_path }}/docker-compose.yml"
      mode: '600'
 
  - name: Deploy nginx configuration file
    template:
      src: templates/default.conf.j2
      dest: "{{ nginx_conf_path }}"
      mode: '640'
 
  - name: Remove current database volume in case of major Robot database PostgreSQL version upgrade
    docker_volume:
      name: grafana_postgres_db_data
      state: absent
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Start PostgreSQL service to restore the back-up in case of major PostgreSQL version upgrade
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      services: postgres_db
      state: present
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Sleep for 3 seconds before restoring the database in case of major PostgreSQL version upgrade
    wait_for:
      timeout: 3
    delegate_to: localhost
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Restore the database back-up in case of major PostgreSQL version upgrade
    shell: |
      docker exec -i postgres_db pg_restore -U {{ robot_db_user }} -v -d {{ robot_db_name }} < {{ postgres_dump_dir }}/{{ postgres_dump_filename }}
    args:
      chdir: "{{ application_path }}"
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Stop docker compose in case of major PostgreSQL version upgrade
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: absent
    when: (local_major_postgres_version != remote_major_postgres_version or force_postgres_upgrade == "Y") and not new_application
 
  - name: Start docker compose
    docker_compose:
      project_src: "{{ application_path }}"
      project_name: grafana
      state: present
```

Nous ne détaillerons pas ici toutes les tâches mais remarquons que :

- **variables** : elles sont exprimées avec la syntaxe [Jinja2](https://palletsprojects.com/p/jinja/) `{{ nom_de_variable }}`. Elles sont définies dans la configuration du playbook que nous détaillerons plus tard, et dans certaines tâches sous la directive `set_fact`.
- **modules** : Ansible fournit des modules qui permettent d'exécuter certaines tâches sans fournir la commande bash exacte. Le module est défini juste sous le nom de la tâche. Nous utilisons ici les modules suivants :
  - [`slurp`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/slurp_module.html) : récupère le contenu d'un fichier (encodé en base64) de l'hôte distant dans une variable.
  - [`set_fact`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/set_fact_module.html) : permet de définir des variables de host et des facts qui peuvent être réutilisés dans le playbook.
  - [`docker_compose`](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_compose_module.html) : exécute les commandes docker compose
  - [`wait_for`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html) : attend pendant un certain temps avant de continuer le playbook (ici nous définissons un temps d'attente de 3 secondes en mettant `timeout : 3`)
  - [`shell`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html) : exécute la commande shell indiquée sur l'hôte cible
  - [`copy`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html) : copie le fichier/dossier src à l'emplacement distant dest.
  - [`template`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html) : développe un fichier template en remplissant les variables Jinja2 avec leurs valeurs, et les copie sur les hôtes cibles de la même manière qu'avec le module copy.
  - [`docker_volume`](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_volume_module.html) : exécute des opérations sur les volumes docker sur l'hôte cible.

- **loops** : utilisé ici avec le module copy, permet de boucler la même tâche sur plusieurs éléments définis par une liste de champs. Ensuite, l'élément est identifié par la variable `{{item}}` dans la définition de la tâche.

  ```yaml
      - name: Deploy configuration folders
        copy:
          src: "{{ item.src }}"
          dest: "{{ item.dest }}"
          mode: "{{ item.mode}}"
        loop:
          - src: config/nginx/ # Terminer le dossier src par / signifie que le contenu du dossier src est copié dans le chemin dest et non le dossier src lui-même
            dest: "{{ nginx_conf_dir }}/"
            mode: '640'
          - src: config/postgres/
            dest: "{{ postgres_conf_dir }}/"
            mode: '644'
          - src: config/prometheus/
            dest: "{{ prometheus_conf_dir }}/"
            mode: '644'
  ```

Pour chaque itération de la boucle, `{item}}` est rempli par les valeurs `src`, `dest`, et `mode` définies pour chaque élément sous la directive `loop`.

- **[conditions](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_conditionals.html)** : définit les conditions dans lesquelles une tâche est exécutée, sous le mot-clé `when`.

- `delegate_to` : indique seulement sur quel hôte autre que celui de l'application/environnement distant nous voulons exécuter la tâche. Ici, nous préférons parfois exécuter sur `localhost` des tâches qui n'ont pas besoin d'être exécutées sur l'hôte cible.

#### Inventaire et variables d'hôte

Afin que le code du playbook puisse fonctionner sur différents ensembles hosts/variables en fonction de l'environnement choisi, nous devons définir un [inventaire](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html).

L'inventaire est défini par un ensemble de dossiers/sous-dossiers dans le dossier `environments`.

Par exemple ici :

```
├── grafana
│ ├─── environments
│ │ ├─── dev
│ │ ├─── group_vars
│ │ └──── all
│ │ │ ├─── cross_env_vars.yml -> ../../../../cross_env_vars.yml
│ │ │ ├─── vars.yml
│ │ │ ├─── vault.yml
│ │ │ └─── versions.yml -> ../../../versions.yml
│ │ └─── hosts
│ │ ├─── prod
│ │ ├─── group_vars
│ │ └──── all
│ │ │ ├─── cross_env_vars.yml -> ../../../../cross_env_vars.yml
│ │ │ ├─── vars.yml
│ │ │ ├─── vault.yml
│ │ │ └─── versions.yml -> ../../../versions.yml
│ │ └─── hosts
│ │ ├── cross_env_vars.yml
│ │ └─── versions.yml
```

##### Configuration de l'environnement

Les deux environnements, `dev` et `prod`, ont la même structure arborescente qui suit les [pratiques suggérées](https://www.digitalocean.com/community/tutorials/how-to-manage-multistage-environments-with-ansible#ansible-recommended-strategy-using-groups-and-multiple-inventories).

- `group_vars/all` : contient la définition des variables requises par l'environnement.
Le nom du dossier `all/` indique que ces variables s'appliquent à chaque hôte défini dans le fichier `hosts` que nous détaillerons plus loin.
Ici, nous pouvons voir que nous avons le contenu suivant :
  - `vars.yml` : contient les variables spécifiques à l'environnement.

    ```yaml
    # FQDN pour les services autres que Grafana
    influxdb_fqdn : influxdb-dev.example.com
    prometheus_fqdn : prometheus-dev.example.com
     
    # Nom de base du certificat
    cert_name : grafana-dev_example_com
     
    # ID du client Grafana Azure AD (pour l'authentification, dépend du serveur)
    grafana_azuread_client_id : xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx
    ```

  - `vault.yml` : contient des variables "vaultées", qui sont des mots de passe chiffrés avec [Ansible vault](https://docs.ansible.com/ansible/latest/vault_guide/index.html). Les données peuvent être publiées sur Gitlab car elles sont effectivement cryptées.

    ```yaml
    vault_influxdb_admin_password: !vault |
            $ANSIBLE_VAULT;1.1;AES256
            613935353262346335373831356134333935373634333530643134393536336333633834643
            3664363237313538323465623265633437323563303636610a6661653262373861626266636
            626263656330623331643063353765383331363331343961646339383663393336393234343
            3537336261346639310a6366616437316630613930373733333035626433346638313635306
            333635656463613031343262383531393336663561383636643636653239386130323935316
            6638613034633162653538626434363137396136366438383839
    
    ```

  - liens symboliques : ils renvoient à des fichiers contenant des variables qui ont les mêmes valeurs dans tous les environnements.

- `hosts` : ce fichier contient la définition des hôtes pour l'application et l'environnement sélectionnés.
Il faut d'abord indiquer le mot clé `[application]`, puis on liste sur la même ligne :
  - le nom de l'application
  - le nom de l'hôte distant
  - l'utilisateur qu'Ansible utilisera pour se connecter au serveur et exécuter le playbook.

  Il peut y avoir ici plusieurs serveurs liés à un environnement mais dans notre cas, nous n'avons qu'un seul serveur défini, par exemple pour l'environnement dev :

  ```ini
  [application]
  grafana ansible_host=grafana-dev.example.com ansible_user=ansible
  ```

  NB_ : nous avons choisi ici le format `.ini` pour ce fichier mais il aurait pu être écrit en YAML, Ansible accepte les deux formats pour définir la configuration du playbook.

##### Configuration globale

Nous avons également deux fichiers qui contiennent des définitions de variables identiques pour tous les environnements. Comme expliqué précédemment, ils sont répliqués dans chaque configuration d'environnement par un **lien symbolique**.

- `cross_env_vars.yml` : contient simplement des définitions de variables statiques qui s'appliquent à tous les environnements.

  **IMPORTANT** : il y a une section qui applique les variables "vaultées" à un autre nom de variable. Ceci a été fait pour éviter de remplir ce fichier avec des mots de passe voûtés, ce qui l'aurait rendu difficile à lire.

```yaml
# Application, dump, configuration and log paths
application_path: /var/opt/grafana
postgres_dump_dir: "{{ application_path }}/dump/postgres"
influxdb_dump_dir: "{{ application_path }}/dump/influxdb"
postgres_conf_dir: "{{ application_path }}/postgres"
prometheus_conf_dir: "{{ application_path }}/prometheus"
prometheus_conf_path: "{{ application_path }}/prometheus/prometheus.yml"
nginx_conf_dir: "{{ application_path }}/nginx"
nginx_conf_path: "{{ application_path }}/nginx/conf.d/default.conf"
nginx_log_path: "{{ application_path }}/nginx/log"
 
# Grafana Azure AD URLs
grafana_azuread_auth_url: "https://login.microsoftonline.com/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx/oauth2/v2.0/authorize"
grafana_azuread_token_url: "https://login.microsoftonline.com/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx/oauth2/v2.0/token"
 
# Robot DB definition
robot_db_name: robot_db
robot_db_user: robot_user
 
# Passwords from vault file
influxdb_admin_password: "{{ vault_influxdb_admin_password }}"
influxdb_admin_token: "{{ vault_influxdb_admin_token }}"
mysql_robot_db_password: "{{ vault_mysql_robot_db_password }}"
mysql_root_password: "{{ vault_mysql_root_password }}"
postgres_user_password: "{{ vault_postgres_user_password }}"
postgres_robot_db_password: "{{ vault_postgres_robot_db_password }}"
pgadmin4_password: "{{ vault_pgadmin4_password }}"
grafana_admin_password: "{{ vault_grafana_admin_password }}"
grafana_azuread_client_secret: "{{ vault_grafana_azuread_client_secret }}"
```

- `versions.yml` : contient les versions des **services** définis dans le fichier docker compose. C'est ce fichier qui doit être mis à jour pour une mise à jour de version des services.

```yaml
influxdb_version: 2.6.0
mysql_version: 8
postgres_version: 15
pgadmin4_version: 2023-03-06-1
grafana_version: 9.3.6
prometheus_version: v2.42.0
node_exporter_version: v1.5.0
nginx_version: 1.23.3
```

#### Templates (configuration dynamique)

```
├── grafana
│ ├─── templates
│ │ ├─── default.conf.j2
│ │ └── docker-compose.yml.j2
```

Après avoir décrit comment les variables étaient définies pour chaque environnement de l'application, nous allons voir maintenant qu'elles peuvent être utilisées pour générer une configuration dynamique.

Pour ce faire, un dossier `templates/` a été créé à la racine du répertoire de l'application, et il contient des fichiers templates incluant des **variables de style Jinja2** `{{ variable }}` alimentées par le processus Ansible.

Habituellement, seuls les deux éléments de configuration suivants dépendront de variables et nécessiteront un fichier template :

- **Docker compose** : le fichier docker-compose.yml déployé sur les serveurs cibles dépend de nombreuses variables, comme les versions des images Docker, les informations d'identification, les chemins d'accès aux applications.
- **Nginx reverse proxy** : quelques éléments dépendent de l'environnement, comme tous les noms de serveurs utilisés par les services applicatifs.

Vous trouverez ci-dessous un extrait d'un fichier `docker-compose.yml` template comme exemple.

```yaml
influxdb:
  # Image is retrieved from Gitlab dependency proxy to avoid Docker hub
  # rate limit exceed; but in most cases, Docker hub image can be used
  image: gitlab.example.com/projects/dependency_proxy/containers/influxdb:{{ influxdb_version }}
  container_name: influxdb
  restart: always
  volumes:
    - influxdb_data:/var/lib/influxdb2
    - influxdb_conf:/etc/influxdb2
    # Volume for back-ups
    - {{ application_path }}/dump/influxdb:/var/local/dump
  networks:
  # InfluxDB UI must be reachable from the outside using the reverse proxy so 'monitoring' network is also added
    - databases
    - monitoring
  environment:
    # InfluxDB will create the specified database and an admin account with the specified credentials.
    # More info: https://github.com/influxdata/influxdata-docker/pull/102
    # https://www.influxdata.com/blog/running-influxdb-2-0-and-telegraf-using-docker/
    DOCKER_INFLUXDB_INIT_MODE: "setup"
    DOCKER_INFLUXDB_INIT_USERNAME: "admin"
    DOCKER_INFUXDB_INIT_PASSWORD: "{{ influxdb_admin_password }}"
    DOCKER_INFLUXDB_INIT_ORG: "monitoring"
    DOCKER_INFLUXDB_INIT_BUCKET: "gitlab"
    DOCKER_INFLUXDB_INIT_ADMIN_TOKEN: "{{ influxdb_admin_token }}"
```

#### Configuration statique

```
├── grafana
│ ├─── config
│ │ ├─── nginx
│ │ ├─── conf.d
│ │ ├─── streams-enabled
│ │ │ └─── influxdb-proxy.conf
│ │ └─── nginx.conf
│ │ ├── postgres
│ │ └─── init
| │ └─── init_robot_db.sh
│ │ └── prometheus
│ │ └── prometheus.yml
```

Nous avons également des services qui nécessiteront une configuration statique (c'est-à-dire ne dépendant d'aucune variable) pour être déployés.

Tout ceci sera situé dans le dossier `config`, avec un répertoire pour chacun des services.

### Encrypter les mots de passe avec Ansible vault

Comme nous l'avons vu précédemment, les informations d'identification sont cryptées avec [Ansible vault](https://docs.ansible.com/ansible/latest/vault_guide/index.html).

Le cryptage est basé sur un mot de passe vault qui est disponible en tant que variable `ANSIBLE_VAULT_PASSWORD` dans les paramètres du projet Gitlab `Applications Deployment`.

Afin d'encrypter un mot de passe, vous pouvez récupérer l'image docker [Cytopia Ansible](https://hub.docker.com/r/cytopia/ansible) et lancer un bash dans le conteneur, ce qui vous évitera d'installer directement Ansible sur votre environnement de travail alors que vous n'en avez pas vraiment besoin. Vous pouvez exécuter les commandes suivantes localement si Docker est installé.

```sh
# Pull the docker image
docker pull cytopia/ansible:latest-tools
 
# Create a container and launch a bash into it
docker run -it --rm cytopia/ansible:latest-tools /bin/bash
 
[INFO] root> /bin/bash
bash-5.1#
```

Dans le conteneur, copiez la valeur de la variable Gitlab `ANSIBLE_VAULT_PASSWORD` dans un fichier nommé `.vault_pass`

Ensuite, définissez la variable d'environnement `ANSIBLE_VAULT_PASSWORD_FILE` et vous serez en mesure d'obtenir l'encryptage de votre mot de passe.

```sh
# Ouvrez le fichier .vault_pass et mettez-y le mot de passe de la chambre forte.
vi .vault_pass
 
# Définir la variable d'environnement ANSIBLE_VAULT_PASSWORD_FILE
export ANSIBLE_VAULT_PASSWORD_FILE=./.vault_pass
 
# Cryptage du mot de passe
ansible-vault encrypt_string 'mon_mot_de_passe'
Encryption successful
!vault |
          $ANSIBLE_VAULT;1.1;AES256
          30356138393065643632353532623533346230393835386563336161373334366333313130333162
          3332643466393664636333303864373339393633333733660a353634613030313030636232613661
          30396636336663303733623762313231316130633338643964383261313835343238313662363433
          6564353666343838330a633339383534386435613464356531303266666339343239333763363536
```

Maintenant, vous pouvez copier le mot de passe dans le fichier de configuration que nous avons vu précédemment.

### Pipeline de déploiement Gitlab CI

#### Flux d'exécution du pipeline

Le flux d'exécution du pipeline CI de Gitlab est censé suivre ces lignes directrices :
- Seules deux étapes existent : `deploy_dev` et `deploy_prod`. 
- Tous les jobs sont manuels
- Les jobs sont créés pour chaque application dont les fichiers sont modifiés. Par exemple, si nous ne modifions que la configuration de l'application Grafana (dossier `grafana/` dans le projet), seul le job de déploiement de Grafana sera créé.
- Sur les dernières étapes de release (commit sur la branche `main` / création d'un tag release) : les jobs de l'environnement dev (étape `deploy_dev`) sont situés avant ceux de l'environnement prod (étape `deploy_prod`) MAIS ils sont autorisés à échouer afin qu'il soit possible de lancer directement les jobs de déploiement de l'environnement prod.

  <img src="static/images/articles/deploy-gitlab-ansible/image-2023-3-10_12-37-28.png" alt="Gitlab CI pipleline" width="600"/>

#### Code Gitlab CI

##### Template de job

Le fichier `.gitlab-ci.yml` du projet de Applications Deployment applique le flux d'exécution précédent, en utilisant le template de job `deploy-playbook-template` à redéfinir pour chaque application/environnement/étape.

```yaml
stages:
  - deploy_dev
  - deploy_prod

variables:
  ANSIBLE_FORCE_COLOR: 'true' # get Ansible colors displayed on Gitlab UI

# We will only only define the latest step that runs on commits pushed on main branch
.rules_ref:
  latest: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

###############################################################################################################
# Jobs definition

# Template job that MUST NOT BE RUN and will be extended
deploy-playbook-template:
  image: ${CI_DEPENDENCY_PROXY_SERVER}/projects/dependency_proxy/containers/cytopia/ansible:latest-tools
  stage: .pre
  rules:
    - when: never
  script:
    # Deduct variables from job name if not provided into target job config
    - if [ "X$APPLICATION_NAME" == "X" ]; then APPLICATION_NAME=$(echo "$CI_JOB_NAME" | awk -F "_|-" '{print $(NF-1)}'); fi
    - echo "INFO - Application to be deployed is $APPLICATION_NAME"
    - if [ "X$ENVIRONMENT" == "X" ]; then ENVIRONMENT=$(echo "$CI_JOB_NAME" | awk -F "_|-" '{print $NF}'); fi
    - echo "INFO - Environment is $ENVIRONMENT"
    # We expect SSH key variables to match the following name : ${APPLICATION_NAME}_${ENVIRONMENT}_SSH_(PRIVATE|PUBLIC)_KEY_BASE64
    - SSH_PRIVATE_KEY_BASE64_VARNAME=$(echo "${APPLICATION_NAME}_${ENVIRONMENT}_SSH_PRIVATE_KEY_BASE64" | tr '[:lower:]' '[:upper:]')
    - eval "SSH_PRIVATE_KEY_BASE64=\${$SSH_PRIVATE_KEY_BASE64_VARNAME}"
    - SSH_PUBLIC_KEY_BASE64_VARNAME=$(echo "${APPLICATION_NAME}_${ENVIRONMENT}_SSH_PUBLIC_KEY_BASE64" | tr '[:lower:]' '[:upper:]')
    - eval "SSH_PUBLIC_KEY_BASE64=\${$SSH_PUBLIC_KEY_BASE64_VARNAME}"
    # Test if required variables are filled
    - |-
      if [ "X$APPLICATION_NAME" == "X" ] || [ "X$ENVIRONMENT" == "X" ] || [ "X$SSH_PRIVATE_KEY_BASE64" == "X" ] || [ "X$SSH_PUBLIC_KEY_BASE64" == "X" ]
      then
        echo "ERROR - One of the required variable was missing. Please check your configuration."
        exit 1
      fi
    # Setup SSH key pair that will be used to authenticate against target server
    - echo "INFO - Adding SSH key pair provided by Gitlab project variables to local SSH configuration"
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY_BASE64" | base64 -d > ~/.ssh/id_ed25519
    - chmod 600 ~/.ssh/id_ed25519
    - echo "$SSH_PUBLIC_KEY_BASE64" | base64 -d > ~/.ssh/id_ed25519.pub
    # Add target host fingerprint to local known_hosts
    # We consider here potentially multiple hosts related to the application (not the current use case though)
    - APPLICATION_SERVER_FQDN_LIST=$(grep $APPLICATION_NAME ${APPLICATION_NAME}/environments/${ENVIRONMENT}/hosts | awk 'BEGIN{RS=" "; FS="ansible_host="}NF>1{print $NF}')
    - echo "INFO - Adding host fingerprints to local known_host for $APPLICATION_NAME application servers FQDN list ($ENVIRONMENT environment) - $(echo $APPLICATION_SERVER_FQDN_LIST)"
    - for APPLICATION_SERVER_FQDN in $APPLICATION_SERVER_FQDN_LIST; do ssh-keyscan -H $APPLICATION_SERVER_FQDN >> ~/.ssh/known_hosts; done
    # Go to application folder from Gitlab project
    - cd $APPLICATION_NAME
    # Setup Ansible vault configuration
    - echo "INFO - Setting up Ansible vault configuration"
    - echo "$ANSIBLE_VAULT_PASSWORD" > .vault_pass
    - export ANSIBLE_VAULT_PASSWORD_FILE=./.vault_pass
    # Launch the Ansible playbook
    - |-
      if [ "$FORCE_POSTGRES_UPGRADE" == "Y" ]
      then
        echo "INFO - Executing the deployment playbook with $ENVIRONMENT environment inventory and the Postgres DB upgrade forced."
        echo "ansible-playbook --extra-vars="force_postgres_upgrade=Y" -i environments/$ENVIRONMENT playbook-deploy.yml"
        ansible-playbook --extra-vars="force_postgres_upgrade=Y" -i environments/$ENVIRONMENT playbook-deploy.yml
      else
        echo "INFO - Executing the deployment playbook with $ENVIRONMENT environment inventory."
        echo "ansible-playbook -i environments/$ENVIRONMENT playbook-deploy.yml"
        ansible-playbook -i environments/$ENVIRONMENT playbook-deploy.yml
      fi

```

Le job permet de fournir la variable FORCE_POSTGRES_UPGRADE (depuis l'interface du pipeline Gitlab par exemple) si l'on veut forcer l'éventuelle mise à jour de la base de données Postgres dans le playbook.

Il est basé sur les variables suivantes qui dépendent de l'application/environnement :

- `APPLICATION_NAME` : de préférence déduit du nom du job (exemple : check-deploy-**grafana**-dev) ou fourni comme variable dans la configuration du job.
- `ENVIRONMENT` : de préférence déduit du nom de la tâche (exemple : check-deploy-grafana-**dev**) ou fourni comme variable dans la configuration de la tâche.
- `SSH_PRIVATE_KEY_BASE64_VARNAME` : obtient la valeur de la variable du projet `[APPLICATION]_[ENVIRONMENT]_SSH_PRIVATE_KEY_BASE64`
- `SSH_PUBLIC_KEY_BASE64_VARNAME` : obtient la valeur de la variable du projet `[APPLICATION]_[ENVIRONMENT]_SSH_PUBLIC_KEY_BASE64`
- `ANSIBLE_VAULT_PASSWORD` : le mot de passe vault principal utilisé pour décrypter les informations d'identification (variable du projet Gitlab)
- `FORCE_POSTGRES_UPGRADE` (optionnel) : pour forcer l'éventuelle mise à jour de la base de données Postgres dans le playbook

##### Jobs de déploiement d'applications

Si nous voulons ajouter une nouvelle application au pipeline, il suffit d'étendre le job caché `deploy-playbook-template`, de définir sous quelles règles les jobs sont lancés, et de mettre le nom de l'application dans les noms des jobs.

```yaml

#######################
# Grafana application #
#######################

latest-deploy-grafana-dev:
  extends: deploy-playbook-template
  stage: deploy_dev
  rules:
    - if: !reference [.rules_ref, latest]
      changes:
        - grafana/**/*
      when: manual
  allow_failure: true
 
latest-deploy-grafana-prod:
  extends: deploy-playbook-template
  stage: deploy_prod
  rules:
    - if: !reference [.rules_ref, latest]
      changes:
        - grafana/**/*
      when: manual
```

#### Sortie du pipeline

Lorsqu'on exécute un pipeline, la sortie sera comme ci-dessous sur l'interface Gitlab (with Ansible colors).

<img src="static/images/articles/deploy-gitlab-ansible/image-2023-3-10_13-1-21.png" alt="Gitlab CI pipleline" width="100%"/>

Nous pouvons remarquer quelques mots clés qui indiquent le statut d'Ansible pour chaque tâche :

- `ok` : indique que la tâche n'a produit aucun changement sur le serveur (par exemple copie d'un fichier identique)
- `changed` : la tâche a produit des changements sur les serveurs (modification d'un fichier, démarrage de docker compose, ...)
- `skipping` : la tâche n'a pas été exécutée car les conditions (when) n'étaient pas remplies
- `fatal` : l'exécution de la tâche a résulté en une erreur ; arrête tout le playbook par défaut, ou l'utilisation de `ignore_errors : true` permet de contourner l'erreur