CREATE VIEW view_articles AS
    SELECT a.id, a.id_article, a.title_slug, a.language, a.title, COALESCE(a.description, ''), COALESCE(a.table_contents, '')
        , a.filename, a.image_path, a.creation_date, a.modified_date, a.read_duration
        , GROUP_CONCAT(t.tag, '|') AS tag_list
        , GROUP_CONCAT(t.tag_slug, '|') AS tag_slug_list
    FROM articles a
    INNER JOIN lk_articles_tags lk ON a.id_article = lk.id_article
    INNER JOIN tags t ON lk.id_tag = t.id_tag AND a.language = t.language
    GROUP BY a.id
    ORDER BY a.language, a.creation_date DESC, a.modified_date DESC;

CREATE VIEW view_tags AS
    SELECT t.id, t.id_tag, t.tag, UPPER(SUBSTR(t.tag, 1, 1)) || SUBSTR(t.tag, 2) as tag_title,
        t.tag_slug, t.language,
        GROUP_CONCAT(a.title, '|') AS article_title_list,
        GROUP_CONCAT(a.title_slug, '|') AS article_title_slug_list,
        COUNT(a.id) as nb_articles
    FROM tags t
    INNER JOIN lk_articles_tags lk ON t.id_tag = lk.id_tag
    INNER JOIN articles a ON lk.id_article = a.id_article AND t.language = a.language
    GROUP BY t.id
    ORDER BY t.language, t.tag_slug;
    --ORDER BY COUNT(a.id) DESC;