CREATE TABLE articles (
    id INTEGER PRIMARY KEY NOT NULL,
    id_article INTEGER NOT NULL,
    title_slug TEXT NOT NULL,
    language TEXT NOT NULL,
    title TEXT NOT NULL,
    description TEXT,
    table_contents TEXT,
    filename TEXT NOT NULL,
    image_path TEXT,
    creation_date TEXT NOT NULL DEFAULT (date()),
    modified_date TEXT NOT NULL DEFAULT (date()),
    read_duration INT NOT NULL,
    UNIQUE (id_article, language),
    UNIQUE (title_slug, language)
);

CREATE TABLE tags (
    id INTEGER PRIMARY KEY NOT NULL,
    id_tag INTEGER NOT NULL,
    tag TEXT NOT NULL,
    tag_slug TEXT NOT NULL,
    language TEXT NOT NULL,
    UNIQUE (id_tag, language),
    UNIQUE (tag, language)
);

CREATE TABLE lk_articles_tags (
    id_article INTEGER NOT NULL,
    id_tag INTEGER NOT NULL,
    PRIMARY KEY (id_article, id_tag),
    FOREIGN KEY (id_article) REFERENCES articles (id_article),
    FOREIGN KEY (id_tag) REFERENCES tags (id_tag)
);
